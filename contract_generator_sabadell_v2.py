#!/usr/bin/python
import zipfile
import sys
import string
import shutil
import os
from subprocess import call

"""
argv1 = full or relative path of the odt template file
argv2 = full or relative path of the temporary folder for extracting content, must end in /
argv3 = operation id
argv4 = full or relative location of the odt destination file
argv5 = name    -- TKBVR001  -index 1
argv6 = surname  -- TKBVR002 --i2
argv7 = surname last  -- TKBVR003 --i3
argv8 = gender (M/F)  -- TKBVR004-5 --i4
argv9 = birthdate  TKBVR006
argv10 = birthplace  TKBVR007
argv11 = birth country TKBVR008
argv12 = birth nation TKBVR009
argv13 = curp TKBVR010
argv14 = rfc          TKBVR011          --index10 
argv15 = identification document TKBVR012    
argv16 = identification document number  TKBVR013          
argv17 = fiel               TKBVR014
argv18 = address             TKBVR015
argv19 = suburb            TKBVR016
argv20 = city   TKBVR017
argv21 = state  TKBVR018
argv22 = cp  TKBVR019
argv23 = country  TKBVR020
argv24 = tel  TKBVR021 --index20
argv25 = cel TKBVR022
argv26 = email  TKBVR023
argv27 = profession  TKBVR024
argv28 = incomes             TKBVR025     
argv29 = PPE (Y/N)  TKBVR026-27
argv30 = PPE_PARENT (Y/N)  TKBVR028-29
argv31 = ppe parent name  TKBVR030
argv32 = ppe parent family member  TKBVR031
argv33 = third activity (Y/N)  TKBVR032-33 --index 29
argv34 = third deposit (Y/N)   TKBVR034-35 --index 30
argv35 = boolean electronic banc acceptance (Y/N)  TKBVR036 --index 31
argv36 = boolean cuenta relacion acceptance (Y/N)  TKBVR037 --index 32
argv37 = boolean pagare sabadell acceptance (Y/N)  TKBVR038 --index 33
argv38 = currentdate TKBVR039 -- index34 (-1) OK IMPLEMENTED 
argv39 = serialCustomer --index 34
argv40 = serialCustomerDate -- index35
argv41 = serialBank -- index36  TKBVR046
argv42 = serialBankDate -- index37  TKBVR047
argv43 = authorization consult credit information - TKBVR040 --index 38
argv44 = aurhotization share information - TKBVR041 -- index 39
argv45 = authorization merchandising  - TKBVR042 -- index 40
argv46 = signing place - TKBVR048 - index41
argv47 = signing date - TKBVR049 - index42
argv48 = cta corr = TKBVR054 - index43
argv49 = cta cbe = TKBRV055 - index44
argv50 = date DAY ONLY TKBVR058- index45
argv51 = date MONTH ONLY TKBVR059- index46
argv52 = date YEAR ONLY TKBVR060- index47
argv53 = credit institution TKBVR061 - index48
argv54 = CLABE destiny TKNVR062 - index49
argv55 = alias account TKBVR063 - index50
argv56 = max amount TKBVR064 - index51
argv57 = date MONTH IN STRING TKVBR068 - index52
argv58 = date PENULTIMATE DIGIT YEAR - TKBVR069 - index53
argv59 = date LAST DIGIT YEAR - TKBVR 070 - index54
argv60 = street (only street) - TKBVR071 - index55
argv61 = no ext - TKBVR072 - index56
argv62 = no int - TKBVR073 - index57
atgv63 = city - TKBVR074 - index58
argv64 = birth date in format dd/MM/yyyy TKBVR078 - index59
argv65 = nationalities - TKBVR075 - index60
argv66 = EEUU nationality (Y/N) TKBVR076-77 - index61
argv67 = other citycenship TKBVR079- index62
argv68 = fullname of beneficent 1 TKBVR084 - index63
argv69 = parentship of beneficent 1 TKBVR085 - index64
argv70 = birthfate of beneficent 1 TKBVR086 - index65
argv71 = percentaje of benef1 TKBVR07 - index66
argv72 = address of b1 TKBVR088 - index67
-
argv73 = fullname of beneficent 2 TKBVR089 - index68
argv74 = parentship of beneficent 2 TKBVR090 - index69
argv75 = birthfate of beneficent 2 TKBVR091 - index70
argv76 = percentaje of benef12 TKBVR92 - index71
argv77 = address of b2 TKBVR093 - index72
-
argv78 = fullname of beneficent 3 TKBVR094 - index73
argv79 = parentship of beneficent 3 TKBVR095 - index74
argv80 = birthfate of beneficent 3 TKBVR096 - index75
argv81 = percentaje of benef3 TKBVR97 - index76
argv82 = address of b3 TKBVR098 - index77
-
argv83 = fullname of beneficent 4 TKBVR099 - index78
argv84 = parentship of beneficent 4 TKBVR100 - index79
argv85 = birthfate of beneficent 4 TKBVR101 - index80
argv86 = percentaje of benef4 TKBV102 - index81
argv87 = address of b4 TKBVR103 - index82
-
argv88 = fullname of beneficent 5 TKBVR104 - index83
argv89 = parentship of beneficent 5 TKBVR105 - index84
argv90 = birthfate of beneficent 5 TKBVR106 - index85
argv91 = percentaje of benef5 TKBVR107 - index86
argv92 = address of b5 TKBVR108 - index87
argv93 = jurisdiction n1 - TKBVR109 - index88
argv94 = rfc tin n1 TKBVR110 - index89
argv95 = reason a n1 TKBVR111 - index90
-
argv96 = jurisdiction n2 - TKBVR109 - index91
argv97 = rfc tin n2 TKBVR110 - index92
argv98 = reason a n2 TKBVR111 - index93
-
argv99 = jurisdiction n3 - TKBVR109 - index94
argv100 = rfc tin n3 TKBVR110 - index95
argv101 = reason a n3 TKBVR111 - index96
-
argv102 = jurisdiction n4 - TKBVR109 - index97
argv103 = rfc tin n4 TKBVR110 - index98
argv104 = reason a n4 TKBVR111 - index99
-
argv105 = tasa interes cuenta - TKBVR121 - index100
argv106 = gat nominal cuenta - TKBVR122 - index101
argv107 = gat real cuenta - TKBVR123 - index102
argv108 = tasa interes pagare - TKBVR124 - index103
argv109 = gat nominal pagare - TKBVR125 - index104
argv110 = gat real pagare - TKBVR126 - index105
"""

    #extracts content to temporal location
try:
    location = sys.argv[2] + sys.argv[3]
    zipContent = zipfile.ZipFile(sys.argv[1])
    zipContent.extractall(location)
    zipContent.close()
        #replace content for correct render
    s = open(location + '/content.xml').read()
        #get variables from cmd
    #print "Opened content from xml"
    listArgs = [sys.argv[5], sys.argv[6], sys.argv[7], sys.argv[8], sys.argv[9], 
                sys.argv[10], sys.argv[11], sys.argv[12], sys.argv[13], sys.argv[14], 
                sys.argv[15], sys.argv[16], sys.argv[17], sys.argv[18], sys.argv[19], 
                sys.argv[20], sys.argv[21], sys.argv[22], sys.argv[23], sys.argv[24], 
                sys.argv[25], sys.argv[26], sys.argv[27], sys.argv[28], sys.argv[29], 
                sys.argv[30], sys.argv[31], sys.argv[32], sys.argv[33], sys.argv[34], 
                sys.argv[35], sys.argv[36], sys.argv[37], sys.argv[38], sys.argv[39], 
                sys.argv[40], sys.argv[41], sys.argv[42], sys.argv[43], sys.argv[44], 
                sys.argv[45], sys.argv[46], sys.argv[47], sys.argv[48], sys.argv[49],
                sys.argv[50], sys.argv[51], sys.argv[52], sys.argv[53], sys.argv[54],
                sys.argv[55], sys.argv[56], sys.argv[57], sys.argv[58], sys.argv[59],
                sys.argv[60], sys.argv[61], sys.argv[62], sys.argv[63], sys.argv[64],
                sys.argv[65], sys.argv[66], sys.argv[67], sys.argv[68], sys.argv[69],
                sys.argv[70], sys.argv[71], sys.argv[72], sys.argv[73], sys.argv[74],
                sys.argv[75], sys.argv[76], sys.argv[77], sys.argv[78], sys.argv[79],
                sys.argv[80], sys.argv[81], sys.argv[82], sys.argv[83], sys.argv[84],
                sys.argv[85], sys.argv[86], sys.argv[87], sys.argv[88], sys.argv[89],
                sys.argv[90], sys.argv[91], sys.argv[92], sys.argv[93], sys.argv[94],
                sys.argv[95], sys.argv[96], sys.argv[97], sys.argv[98], sys.argv[99],
                sys.argv[100], sys.argv[101], sys.argv[102], sys.argv[103], sys.argv[104],
                sys.argv[105], sys.argv[106], sys.argv[107], sys.argv[108], sys.argv[109],
                sys.argv[110], sys.argv[111]
                ]

    for x in range(len(listArgs)):
        var_temp = listArgs[x]
        if var_temp == 'NA':
            var_temp = ''
        var_temp = var_temp.replace('_',' ')
        listArgs[x] = var_temp
        print "Assigned ",var_temp," at index", x
    #end of formatting argument list
    s = s.replace('TKBVR001', listArgs[0])
    s = s.replace('TKBVR002', listArgs[1])
    s = s.replace('TKBVR003', listArgs[2])
    if listArgs[3] == 'M':
        s = s.replace('TKBVR004','')
        s = s.replace('TKBVR005','X')
    else:
        s = s.replace('TKBVR004','X')
        s = s.replace('TKBVR005','')
    s = s.replace('TKBVR006', listArgs[4])
    s = s.replace('TKBVR007',listArgs[5]) #Birthplace - ok so far
    s = s.replace('TKBVR008',listArgs[6]) 
    s = s.replace('TKBVR009',listArgs[7]) 
    s = s.replace('TKBVR010',listArgs[8]) 
    s = s.replace('TKBVR011',listArgs[9]) 
    s = s.replace('TKBVR012',listArgs[10]) 
    s = s.replace('TKBVR013',listArgs[11]) 
    s = s.replace('TKBVR014',listArgs[12]) 
    s = s.replace('TKBVR015',listArgs[13]) 
    s = s.replace('TKBVR016',listArgs[14]) 
    s = s.replace('TKBVR017',listArgs[15]) 
    s = s.replace('TKBVR018',listArgs[16]) 
    s = s.replace('TKBVR019',listArgs[17]) 
    s = s.replace('TKBVR020',listArgs[18]) 
    s = s.replace('TKBVR021',listArgs[19]) 
    s = s.replace('TKBVR022',listArgs[20]) 
    s = s.replace('TKBVR023',listArgs[21]) 
    s = s.replace('TKBVR024',listArgs[22]) 
    s = s.replace('TKBVR025',listArgs[23])  #income monthly
    if listArgs[24] == 'Y':
        s = s.replace('TKBVR026','X')
        s = s.replace('TKBVR027','')
    else:
        s = s.replace('TKBVR026','')
        s = s.replace('TKBVR027','X')
    if listArgs[25] == 'Y':
        s = s.replace('TKBVR028','X')
        s = s.replace('TKBVR029','')
    else:
        s = s.replace('TKBVR028','')
        s = s.replace('TKBVR029','X')
    s = s.replace('TKBVR030',listArgs[26]) 
    s = s.replace('TKBVR031',listArgs[27])
    if listArgs[28] == 'Y':
        s = s.replace('TKBVR032','X')
        s = s.replace('TKBVR033','')
    else:
        s = s.replace('TKBVR032','')
        s = s.replace('TKBVR033','X')
    if listArgs[29] == 'Y':
        s = s.replace('TKBVR034','X')
        s = s.replace('TKBVR035','')
    else:
        s = s.replace('TKBVR034','')
        s = s.replace('TKBVR035','X')
    #Begin with acceptance at index 31
    if listArgs[30] == 'Y':
        s = s.replace('TKBVR036','X')
    else:
        s = s.replace('TKBVR036','')
    if listArgs[31] == 'Y':
        s = s.replace('TKBVR037','X')
    else:
        s = s.replace('TKBVR037','')
    if listArgs[32] == 'Y':
        s = s.replace('TKBVR038','X')
    else:
        s = s.replace('TKBVR038','')
    s = s.replace('TKBVR039',listArgs[33]) #current date, ok so far  
    s = s.replace('TKBVR046',listArgs[36])
    s = s.replace('TKBVR047',listArgs[37])
    if listArgs[38] == 'Y':
        s = s.replace('TKBVR040', listArgs[34])
        s = s.replace('TKBVR041', listArgs[35])
    else:
        s = s.replace('TKBVR040', '')
        s = s.replace('TKBVR041', '')
    if listArgs[39] == 'Y':
        s = s.replace('TKBVR042', listArgs[34])
        s = s.replace('TKBVR043', listArgs[35])
    else:
        s = s.replace('TKBVR042', '')
        s = s.replace('TKBVR043', '')
    if listArgs[40] == 'Y':
        s = s.replace('TKBVR044', listArgs[34])
        s = s.replace('TKBVR045', listArgs[35])
    else:
        s = s.replace('TKBVR044', '')
        s = s.replace('TKBVR045', '')
    s = s.replace('TKBVR048',listArgs[41])
    s = s.replace('TKBVR049',listArgs[42])
    s = s.replace('TKBVR050', listArgs[34])
    s = s.replace('TKBVR051', listArgs[35])
    s = s.replace('TKBVR052', listArgs[36])
    s = s.replace('TKBVR053', listArgs[37])
    s = s.replace('TKBVR054', listArgs[43])
    s = s.replace('TKBVR055', listArgs[44])
    s = s.replace('TKBVR056', listArgs[43])
    s = s.replace('TKBVR057', listArgs[43])
    s = s.replace('TKBVR058', listArgs[45])
    s = s.replace('TKBVR059', listArgs[46])
    s = s.replace('TKBVR060', listArgs[47])
    s = s.replace('TKBVR061', listArgs[48])
    s = s.replace('TKBVR062', listArgs[49])
    s = s.replace('TKBVR063', listArgs[50])
    s = s.replace('TKBVR064', listArgs[51])
    s = s.replace('TKBVR065', listArgs[34])
    s = s.replace('TKBVR066', listArgs[35])
    s = s.replace('TKBVR067', listArgs[45])
    s = s.replace('TKBVR068', listArgs[52])
    s = s.replace('TKBVR069', listArgs[53])
    s = s.replace('TKBVR070', listArgs[54])
    s = s.replace('TKBVR071', listArgs[55])
    s = s.replace('TKBVR072', listArgs[56])
    s = s.replace('TKBVR073', listArgs[57])
    s = s.replace('TKBVR074', listArgs[58])
    s = s.replace('TKBVR078', listArgs[59])
    s = s.replace('TKBVR075', listArgs[60])
    if listArgs[61] == 'Y':
        s = s.replace('TKBVR076', 'X')
        s = s.replace('TKBVR077', '')
    else:
        s = s.replace('TKBVR076', '')
        s = s.replace('TKBVR077', 'X')
    s = s.replace('TKBVR079', listArgs[62])
    s = s.replace('TKBVR080', listArgs[34])
    s = s.replace('TKBVR081', listArgs[35])
    s = s.replace('TKBVR082', listArgs[34])
    s = s.replace('TKBVR083', listArgs[35])
    s = s.replace('TKBVR084', listArgs[63])
    paren1 = listArgs[64]
    if paren1 == 'HERMANO A':
        s = s.replace('TKBVR085', 'HERMANO(A)')
    elif paren1 == 'MEDIO HERMANO A':
        s = s.replace('TKBVR085', 'MEDIO HERMANO(A)')
    elif paren1 == 'HIJO A':
        s = s.replace('TKBVR085', 'HIJO(A)')
    else:
        s = s.replace('TKBVR085', paren1)
    #s = s.replace('TKBVR085', listArgs[64])
    s = s.replace('TKBVR086', listArgs[65])
    if listArgs[66] == 'NA' or listArgs[66] == '':
        s = s.replace('TKBVR087', listArgs[66])
    else:
        s = s.replace('TKBVR087', listArgs[66] + '%')
    s = s.replace('TKBVR088', listArgs[67])
    s = s.replace('TKBVR089', listArgs[68])
    paren2 = listArgs[69]
    print 'Parent 2'
    print paren2
    if 'HERMANO A' in paren2:
        s = s.replace('TKBVR090', 'HERMANO(A)')
    elif 'MEDIO HERMANO A' in paren2:
        s = s.replace('TKBVR090', 'MEDIO HERMANO(A)')
    elif 'HIJO A' in paren2:
        s = s.replace('TKBVR090', 'HIJO(A)')
    else:
        s = s.replace('TKBVR090', paren2)
    s = s.replace('TKBVR091', listArgs[70])
    if listArgs[71] == 'NA' or listArgs[71] == '':
        s = s.replace('TKBVR092', listArgs[71])
    else:
        s = s.replace('TKBVR092', listArgs[71] + '%')
    s = s.replace('TKBVR093', listArgs[72])
    s = s.replace('TKBVR094', listArgs[73])
    paren3 = listArgs[74]
    if 'HERMANO A' in paren3:
        s = s.replace('TKBVR095', 'HERMANO(A)')
    elif 'MEDIO HERMANO A' in paren3:
        s = s.replace('TKBVR095', 'MEDIO HERMANO(A)')
    elif 'HIJO A' in paren3:
        s = s.replace('TKBVR095', 'HIJO(A)')
    else:
        s = s.replace('TKBVR095', paren3)
    s = s.replace('TKBVR096', listArgs[75])
    if listArgs[76] == 'NA' or listArgs[76] == '':
        s = s.replace('TKBVR097', listArgs[76])
    else:
        s = s.replace('TKBVR097', listArgs[76] + '%')
    s = s.replace('TKBVR098', listArgs[77])
    s = s.replace('TKBVR099', listArgs[78])
    paren4 = listArgs[79]
    if 'HERMANO A' in paren4:
        s = s.replace('TKBVR100', 'HERMANO(A)')
    elif 'MEDIO HERMANO A' in paren4:
        s = s.replace('TKBVR100', 'MEDIO HERMANO(A)')
    elif 'HIJO A' in paren4:
        s = s.replace('TKBVR100', 'HIJO(A)')
    else:
        s = s.replace('TKBVR100', paren4)
    s = s.replace('TKBVR101', listArgs[80])
    if listArgs[81] == 'NA' or listArgs[81] == '':
        s = s.replace('TKBVR102', listArgs[81])
    else:
        s = s.replace('TKBVR102', listArgs[81] + '%')
    s = s.replace('TKBVR103', listArgs[82])
    s = s.replace('TKBVR104', listArgs[83])
    paren5 = listArgs[84]
    if 'HERMANO A' in paren5:
        s = s.replace('TKBVR105', 'HERMANO(A)')
    elif 'MEDIO HERMANO A' in paren5:
        s = s.replace('TKBVR105', 'MEDIO HERMANO(A)')
    elif 'HIJO A' in paren5:
        s = s.replace('TKBVR105', 'HIJO(A)')
    else:
        s = s.replace('TKBVR105', paren5)
    s = s.replace('TKBVR106', listArgs[85])
    if listArgs[86] == 'NA' or listArgs[86] == '':
        s = s.replace('TKBVR107', listArgs[86])
    else:
        s = s.replace('TKBVR107', listArgs[86] + '%')
    s = s.replace('TKBVR108', listArgs[87])
    s = s.replace('TKBVR109', listArgs[88])
    s = s.replace('TKBVR110', listArgs[89])
    s = s.replace('TKBVR111', listArgs[90])
    s = s.replace('TKBVR112', listArgs[91])
    s = s.replace('TKBVR113', listArgs[92])
    s = s.replace('TKBVR114', listArgs[93])
    s = s.replace('TKBVR115', listArgs[94])
    s = s.replace('TKBVR116', listArgs[95])
    s = s.replace('TKBVR117', listArgs[96])
    s = s.replace('TKBVR118', listArgs[97])
    s = s.replace('TKBVR119', listArgs[98])
    s = s.replace('TKBVR120', listArgs[99])
    s = s.replace('TKBVR121', listArgs[100])
    s = s.replace('TKBVR122', listArgs[101])
    s = s.replace('TKBVR123', listArgs[102])
    s = s.replace('TKBVR124', listArgs[103])
    s = s.replace('TKBVR125', listArgs[104])
    s = s.replace('TKBVR126', listArgs[105])
    if listArgs[106] == 'NA' or listArgs[106] == '':
        s = s.replace('	', ' ')
    else:
    	s = s.replace('TKBVR130', listArgs[106])
    f = open(location + '/content.xml', 'w')
    f.write(s)
    f.close()
    #print "content written"
        #zip content again
    shutil.make_archive(sys.argv[4], 'zip', location)
    #print "Zip made"
        #delete zip file extension
    os.rename(sys.argv[4] + '.zip', sys.argv[4])
    #print "Document renamed"
    shutil.rmtree(location)
    #print "Document removed"
        #convert document
    #print "Calling unoconv -f pdf "
    print sys.argv[4]
    call(["unoconv", "-f", "pdf", sys.argv[4]])
        #remove odt file
    os.remove(sys.argv[4])
except:
    print "Unexpected error converting the template file to PDF:", sys.exc_info()[0]
    raise