/**
 * FirmanteInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.datacontract.autoSigned;

import org.datacontract.autoSigned.FirmanteInfo;

public class FirmanteInfo  implements java.io.Serializable {
    private java.lang.String apMaterno;

    private java.lang.String apPaterno;

    private java.lang.String email;

    private java.lang.String imagen;

    private java.lang.String leyenda;

    private java.lang.String nombres;

    private java.lang.Integer pagina;

    private java.lang.Integer posX;

    private java.lang.Integer posY;

    private java.lang.String RFC;

    public FirmanteInfo() {
    }

    public FirmanteInfo(
           java.lang.String apMaterno,
           java.lang.String apPaterno,
           java.lang.String email,
           java.lang.String imagen,
           java.lang.String leyenda,
           java.lang.String nombres,
           java.lang.Integer pagina,
           java.lang.Integer posX,
           java.lang.Integer posY,
           java.lang.String RFC) {
           this.apMaterno = apMaterno;
           this.apPaterno = apPaterno;
           this.email = email;
           this.imagen = imagen;
           this.leyenda = leyenda;
           this.nombres = nombres;
           this.pagina = pagina;
           this.posX = posX;
           this.posY = posY;
           this.RFC = RFC;
    }


    /**
     * Gets the apMaterno value for this FirmanteInfo.
     * 
     * @return apMaterno
     */
    public java.lang.String getApMaterno() {
        return apMaterno;
    }


    /**
     * Sets the apMaterno value for this FirmanteInfo.
     * 
     * @param apMaterno
     */
    public void setApMaterno(java.lang.String apMaterno) {
        this.apMaterno = apMaterno;
    }


    /**
     * Gets the apPaterno value for this FirmanteInfo.
     * 
     * @return apPaterno
     */
    public java.lang.String getApPaterno() {
        return apPaterno;
    }


    /**
     * Sets the apPaterno value for this FirmanteInfo.
     * 
     * @param apPaterno
     */
    public void setApPaterno(java.lang.String apPaterno) {
        this.apPaterno = apPaterno;
    }


    /**
     * Gets the email value for this FirmanteInfo.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this FirmanteInfo.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the imagen value for this FirmanteInfo.
     * 
     * @return imagen
     */
    public java.lang.String getImagen() {
        return imagen;
    }


    /**
     * Sets the imagen value for this FirmanteInfo.
     * 
     * @param imagen
     */
    public void setImagen(java.lang.String imagen) {
        this.imagen = imagen;
    }


    /**
     * Gets the leyenda value for this FirmanteInfo.
     * 
     * @return leyenda
     */
    public java.lang.String getLeyenda() {
        return leyenda;
    }


    /**
     * Sets the leyenda value for this FirmanteInfo.
     * 
     * @param leyenda
     */
    public void setLeyenda(java.lang.String leyenda) {
        this.leyenda = leyenda;
    }


    /**
     * Gets the nombres value for this FirmanteInfo.
     * 
     * @return nombres
     */
    public java.lang.String getNombres() {
        return nombres;
    }


    /**
     * Sets the nombres value for this FirmanteInfo.
     * 
     * @param nombres
     */
    public void setNombres(java.lang.String nombres) {
        this.nombres = nombres;
    }


    /**
     * Gets the pagina value for this FirmanteInfo.
     * 
     * @return pagina
     */
    public java.lang.Integer getPagina() {
        return pagina;
    }


    /**
     * Sets the pagina value for this FirmanteInfo.
     * 
     * @param pagina
     */
    public void setPagina(java.lang.Integer pagina) {
        this.pagina = pagina;
    }


    /**
     * Gets the posX value for this FirmanteInfo.
     * 
     * @return posX
     */
    public java.lang.Integer getPosX() {
        return posX;
    }


    /**
     * Sets the posX value for this FirmanteInfo.
     * 
     * @param posX
     */
    public void setPosX(java.lang.Integer posX) {
        this.posX = posX;
    }


    /**
     * Gets the posY value for this FirmanteInfo.
     * 
     * @return posY
     */
    public java.lang.Integer getPosY() {
        return posY;
    }


    /**
     * Sets the posY value for this FirmanteInfo.
     * 
     * @param posY
     */
    public void setPosY(java.lang.Integer posY) {
        this.posY = posY;
    }


    /**
     * Gets the RFC value for this FirmanteInfo.
     * 
     * @return RFC
     */
    public java.lang.String getRFC() {
        return RFC;
    }


    /**
     * Sets the RFC value for this FirmanteInfo.
     * 
     * @param RFC
     */
    public void setRFC(java.lang.String RFC) {
        this.RFC = RFC;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FirmanteInfo)) return false;
        FirmanteInfo other = (FirmanteInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.apMaterno==null && other.getApMaterno()==null) || 
             (this.apMaterno!=null &&
              this.apMaterno.equals(other.getApMaterno()))) &&
            ((this.apPaterno==null && other.getApPaterno()==null) || 
             (this.apPaterno!=null &&
              this.apPaterno.equals(other.getApPaterno()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.imagen==null && other.getImagen()==null) || 
             (this.imagen!=null &&
              this.imagen.equals(other.getImagen()))) &&
            ((this.leyenda==null && other.getLeyenda()==null) || 
             (this.leyenda!=null &&
              this.leyenda.equals(other.getLeyenda()))) &&
            ((this.nombres==null && other.getNombres()==null) || 
             (this.nombres!=null &&
              this.nombres.equals(other.getNombres()))) &&
            ((this.pagina==null && other.getPagina()==null) || 
             (this.pagina!=null &&
              this.pagina.equals(other.getPagina()))) &&
            ((this.posX==null && other.getPosX()==null) || 
             (this.posX!=null &&
              this.posX.equals(other.getPosX()))) &&
            ((this.posY==null && other.getPosY()==null) || 
             (this.posY!=null &&
              this.posY.equals(other.getPosY()))) &&
            ((this.RFC==null && other.getRFC()==null) || 
             (this.RFC!=null &&
              this.RFC.equals(other.getRFC())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getApMaterno() != null) {
            _hashCode += getApMaterno().hashCode();
        }
        if (getApPaterno() != null) {
            _hashCode += getApPaterno().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getImagen() != null) {
            _hashCode += getImagen().hashCode();
        }
        if (getLeyenda() != null) {
            _hashCode += getLeyenda().hashCode();
        }
        if (getNombres() != null) {
            _hashCode += getNombres().hashCode();
        }
        if (getPagina() != null) {
            _hashCode += getPagina().hashCode();
        }
        if (getPosX() != null) {
            _hashCode += getPosX().hashCode();
        }
        if (getPosY() != null) {
            _hashCode += getPosY().hashCode();
        }
        if (getRFC() != null) {
            _hashCode += getRFC().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FirmanteInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "FirmanteInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apMaterno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "ApMaterno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apPaterno");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "ApPaterno"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "Email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imagen");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "Imagen"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leyenda");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "Leyenda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombres");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "Nombres"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pagina");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "Pagina"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posX");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "PosX"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("posY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "PosY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RFC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Doc2SignLite_AutoSigned", "RFC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
