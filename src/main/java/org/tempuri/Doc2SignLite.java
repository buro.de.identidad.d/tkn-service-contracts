/**
 * Doc2SignLite.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface Doc2SignLite extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_IDoc2SignLiteAddress();

    public org.tempuri.IDoc2SignLite getBasicHttpBinding_IDoc2SignLite() throws javax.xml.rpc.ServiceException;

    public org.tempuri.IDoc2SignLite getBasicHttpBinding_IDoc2SignLite(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
