/**
 * Doc2SignLiteLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

import org.springframework.beans.factory.annotation.Value;

public class Doc2SignLiteLocator extends org.apache.axis.client.Service implements org.tempuri.Doc2SignLite {

    @Value("${karalundiCertV3.service}")
    private String service;

    public Doc2SignLiteLocator() {
    }

    public Doc2SignLiteLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Doc2SignLiteLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpBinding_IDoc2SignLite
    private java.lang.String BasicHttpBinding_IDoc2SignLite_address = "https://wsautosign.doc2sign.com/Doc2SignLite.svc";

    public java.lang.String getBasicHttpBinding_IDoc2SignLiteAddress() {
        return BasicHttpBinding_IDoc2SignLite_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BasicHttpBinding_IDoc2SignLiteWSDDServiceName = "BasicHttpBinding_IDoc2SignLite";

    public java.lang.String getBasicHttpBinding_IDoc2SignLiteWSDDServiceName() {
        return BasicHttpBinding_IDoc2SignLiteWSDDServiceName;
    }

    public void setBasicHttpBinding_IDoc2SignLiteWSDDServiceName(java.lang.String name) {
        BasicHttpBinding_IDoc2SignLiteWSDDServiceName = name;
    }

    public org.tempuri.IDoc2SignLite getBasicHttpBinding_IDoc2SignLite() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpBinding_IDoc2SignLite_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpBinding_IDoc2SignLite(endpoint);
    }

    public org.tempuri.IDoc2SignLite getBasicHttpBinding_IDoc2SignLite(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.BasicHttpBinding_IDoc2SignLiteStub _stub = new org.tempuri.BasicHttpBinding_IDoc2SignLiteStub(portAddress, this);
            _stub.setPortName(getBasicHttpBinding_IDoc2SignLiteWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpBinding_IDoc2SignLiteEndpointAddress(java.lang.String address) {
        BasicHttpBinding_IDoc2SignLite_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.tempuri.IDoc2SignLite.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.BasicHttpBinding_IDoc2SignLiteStub _stub = new org.tempuri.BasicHttpBinding_IDoc2SignLiteStub(new java.net.URL(BasicHttpBinding_IDoc2SignLite_address), this);
                _stub.setPortName(getBasicHttpBinding_IDoc2SignLiteWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BasicHttpBinding_IDoc2SignLite".equals(inputPortName)) {
            return getBasicHttpBinding_IDoc2SignLite();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "Doc2SignLite");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "BasicHttpBinding_IDoc2SignLite"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BasicHttpBinding_IDoc2SignLite".equals(portName)) {
            setBasicHttpBinding_IDoc2SignLiteEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
