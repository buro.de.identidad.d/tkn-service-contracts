package org.tempuri;

public class IDoc2SignLiteProxy implements org.tempuri.IDoc2SignLite {
  private String _endpoint = null;
  private org.tempuri.IDoc2SignLite iDoc2SignLite = null;
  
  public IDoc2SignLiteProxy() {
    _initIDoc2SignLiteProxy();
  }
  
  public IDoc2SignLiteProxy(String endpoint) {
    _endpoint = endpoint;
    _initIDoc2SignLiteProxy();
  }
  
  private void _initIDoc2SignLiteProxy() {
    try {
      iDoc2SignLite = (new org.tempuri.Doc2SignLiteLocator()).getBasicHttpBinding_IDoc2SignLite();
      if (iDoc2SignLite != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iDoc2SignLite)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iDoc2SignLite)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iDoc2SignLite != null)
      ((javax.xml.rpc.Stub)iDoc2SignLite)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.IDoc2SignLite getIDoc2SignLite() {
    if (iDoc2SignLite == null)
      _initIDoc2SignLiteProxy();
    return iDoc2SignLite;
  }
  
  public java.lang.String load(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador, java.lang.String nombres, java.lang.String apPaterno, java.lang.String apMaterno, java.lang.String RFC, java.lang.String email, java.lang.String documentoBase64, java.lang.Boolean mostrarFirmas, java.lang.String imagenFirma) throws java.rmi.RemoteException{
    if (iDoc2SignLite == null)
      _initIDoc2SignLiteProxy();
    return iDoc2SignLite.load(usuarioServicio, passwordServicio, identificador, nombres, apPaterno, apMaterno, RFC, email, documentoBase64, mostrarFirmas, imagenFirma);
  }
  
  public java.lang.String loadMultiple(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador, org.datacontract.autoSigned.FirmanteInfo[] firmantes, org.datacontract.autoSigned.FirmanteInfo firmaEmpresa, java.lang.String documentoBase64, java.lang.Integer mostrarFirmas) throws java.rmi.RemoteException{
    if (iDoc2SignLite == null)
      _initIDoc2SignLiteProxy();
    return iDoc2SignLite.loadMultiple(usuarioServicio, passwordServicio, identificador, firmantes, firmaEmpresa, documentoBase64, mostrarFirmas);
  }
  
  public java.lang.String GETDocFirmado(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador) throws java.rmi.RemoteException{
    if (iDoc2SignLite == null)
      _initIDoc2SignLiteProxy();
    return iDoc2SignLite.GETDocFirmado(usuarioServicio, passwordServicio, identificador);
  }
  
  public java.lang.String GETNOM(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador) throws java.rmi.RemoteException{
    if (iDoc2SignLite == null)
      _initIDoc2SignLiteProxy();
    return iDoc2SignLite.GETNOM(usuarioServicio, passwordServicio, identificador);
  }
  
  public org.datacontract.autoSigned.CertificadoInfo[] GETCertificado(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador) throws java.rmi.RemoteException{
    if (iDoc2SignLite == null)
      _initIDoc2SignLiteProxy();
    return iDoc2SignLite.GETCertificado(usuarioServicio, passwordServicio, identificador);
  }
  
  public java.lang.String validaNOM(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String documentoBase64, java.lang.String NOMBase64) throws java.rmi.RemoteException{
    if (iDoc2SignLite == null)
      _initIDoc2SignLiteProxy();
    return iDoc2SignLite.validaNOM(usuarioServicio, passwordServicio, documentoBase64, NOMBase64);
  }
  
  
}