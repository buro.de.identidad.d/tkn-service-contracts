package com.teknei.bid.command.impl.contract;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.service.ContractDatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersistContractCommand implements Command {

    @Autowired
    private ContractDatabaseService contractDatabaseService;
    private static final Logger log = LoggerFactory.getLogger(PersistContractCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".execute ");
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setId(request.getId());
        try {
            contractDatabaseService.alterRecord(request.getId(), true, request.getUsername());
            commandResponse.setStatus(Status.CONTRACT_DB_OK);
        } catch (Exception e) {
            log.error("Error in persistContractCommand for : {} with message: {}", request, e.getMessage());
            commandResponse.setStatus(Status.CONTRACT_DB_ERROR);
        }
        return commandResponse;
    }


}
