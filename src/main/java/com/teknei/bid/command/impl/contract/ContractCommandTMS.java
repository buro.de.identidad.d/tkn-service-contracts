package com.teknei.bid.command.impl.contract;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.RequestType;
import com.teknei.bid.command.Status;

@Component
public class ContractCommandTMS implements Command {
	
	@Autowired
    @Qualifier(value = "storeTasContractCommand")
    private Command storeTasContractCommand;
	@Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;
	
	private static final Logger log = LoggerFactory.getLogger(ContractCommandTMS.class);

	@Override
	public CommandResponse execute(CommandRequest request) {
		
		byte [] contract = null;
		byte [] signedContract = null;
		
		if(request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED)) {
			
			contract = request.getContract();
			
		}else if(request.getRequestType().equals(RequestType.CONTRACT_REQUEST_SIGNED)){
			
			signedContract = request.getSignedContract();
			
		}
		
		CommandRequest tasRequest = new CommandRequest();
		
        tasRequest.setId(request.getId());
        
        if (request.getRequestType().equals(RequestType.CONTRACT_REQUEST_UNSIGNED)) {
        	
            tasRequest.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            tasRequest.setContract(contract);
            
        } else {
        	
        	tasRequest.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED);
            tasRequest.setSignedContract(signedContract);
            
        }
        
        CommandResponse tasResponse = storeTasContractCommand.execute(tasRequest);
        
        log.info("LBMV::: Guardando en TAS");
        
        if (tasResponse.getStatus().equals(Status.CONTRACT_TAS_UNSIGNED_ERROR) || tasResponse.getStatus().equals(Status.CONTRACT_TAS_SIGNED_ERROR)) {
        	
            saveStatus(request.getId(), tasResponse.getStatus(), request.getUsername());
            tasResponse.setDesc(String.valueOf(tasResponse.getStatus().getValue()));
            tasResponse.setStatus(Status.CONTRACT_ERROR);
            
        }
		
        return tasResponse;
	}
	
	private CommandResponse saveStatus(Long id, Status status, String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".saveStatus ");
        CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setRequestStatus(status);
        request.setUsername(username);
        CommandResponse response = statusCommand.execute(request);
        return response;
    }

}
