package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 08/08/2017.
 */
@Data
public class PersonData implements Serializable {

    private String name;
    private String surename;
    private String surenameLast;
    private String lastNames;
    private String personalNumber;

}