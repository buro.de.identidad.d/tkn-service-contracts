package com.teknei.bid.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class TMSDocumentosDTO implements Serializable {
	
	private Long id;
	private String indexLeft = "";
    private boolean confirma;
    
    @Override
	public String toString() {
    	return "TMSDocumentosDTO [ id = " + id + ", indexLeft = " + indexLeft + ", confirma = " + confirma + " ]";
    	
    }

}
