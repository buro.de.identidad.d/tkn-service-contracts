package com.teknei.bid.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class CertCreatedDTO implements Serializable {

    private String serial;
    private Integer exitCode;
    private String username;

}