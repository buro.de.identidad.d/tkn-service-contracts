package com.teknei.bid.controller.rest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.contracts.dto.ContractItemDto;
import com.contracts.dto.RequestContractDto;
import com.contracts.service.ContractService;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.RequestType;
import com.teknei.bid.command.Status;
import com.teknei.bid.controller.rest.crypto.Decrypt;
import com.teknei.bid.dto.ContractDemoDTO;
import com.teknei.bid.dto.ContractSignedDTO;
import com.teknei.bid.dto.SignContractRequestDTO;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.service.ContractSignService;
import com.teknei.bid.service.KaralundiSignService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/contract")
public class ContractController {
	
	@Autowired
    private Decrypt decrypt;
	
    @Autowired
    private KaralundiSignService karalundiSignService;
	
	private static final String[] FINGER_FIRMA = {"li"};

    @Autowired
    @Qualifier("contractCommand")
    private Command contractCommand;
    
    @Autowired
    @Qualifier("contractCommandWithCerts")
    private Command contractCommandWithCerts;
    
   
    
    

    private static final Logger log = LoggerFactory.getLogger(ContractController.class);
    
    @RequestMapping(value = "/contrato/sign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> signContract(@RequestBody SignContractRequestDTO contractRequestDTO) {
        log.debug("Requesting /contrato/sign: {}", contractRequestDTO);
        try {
            CommandRequest commandRequest = new CommandRequest();
            commandRequest.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED);
            commandRequest.setData(contractRequestDTO.getBase64Finger());
            commandRequest.setData2(contractRequestDTO.getContentType());
            commandRequest.setId(contractRequestDTO.getOperationId());
            commandRequest.setUsername(contractRequestDTO.getUsername());
            commandRequest.setScanId(contractRequestDTO.getHash());
            CommandResponse response = contractCommand.execute(commandRequest);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contrato/{id}/{username}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContract(@PathVariable("id") Long id, @PathVariable("username") String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContract ");
        byte[] contract = new byte[0];
        try {
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                contract = response.getContract();
                return new ResponseEntity<>(contract, HttpStatus.OK);
            } else {
                return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(contract, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract pre filled for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contractWithCert/{id}/{username}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getUnsignedContractWithCerts(@PathVariable("id") Long id, @PathVariable("username") String username){
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts ");
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts id      :"+id);
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts username:"+username);
        byte[] contract = new byte[0];
        try{
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_PREFILLED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommandWithCerts.execute(request);
            //log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus():"+response.getStatus());
            if (response.getStatus().equals(Status.CONTRACT_OK)) 
            {
            	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus(): ENTRO A TRUE");
                contract = response.getContract();
                return new ResponseEntity<>(contract, HttpStatus.OK);
            }
            else 
            {
            	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractWithCerts response.getStatus(): ENTRO A FALSE");
                return new ResponseEntity<>((byte[]) null, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(contract, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Generates contract for the current customer given by id", response = byte[].class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/contratoB64/{id}/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getUnsignedContractB64(@PathVariable("id") Long id, @PathVariable("username") String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContractB64 ");
        byte[] contract = new byte[0];
        String contractB64 = "";
        JSONObject jsonObject = new JSONObject();
        try {
            CommandRequest request = new CommandRequest();
            request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            request.setId(id);
            request.setUsername(username);
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                contract = response.getContract();
                contractB64 = Base64Utils.encodeToString(contract);
                jsonObject.put("contract", contractB64);
                return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
            } else {
                jsonObject.put("contract", "");
                return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error generating contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Uploads contract signed and stores in the document manager")
    @RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addPlainSignedContract(@RequestBody ContractSignedDTO contractSignedDTO) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addPlainSignedContract ");
    	
        try {
            CommandRequest request = new CommandRequest();
            
            request.setId(contractSignedDTO.getOperationId());
            request.setUsername(contractSignedDTO.getUsername());
            request.setContract(Base64Utils.decodeFromString(contractSignedDTO.getFileB64()));
            
            CommandResponse response = contractCommand.execute(request);
            
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
            	
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
                
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
        	
            log.error("Error adding signed contract with message: {}", e.getMessage());
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Deprecated
    @ApiOperation(value = "Adds signed contract to the document casefile", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "If the contract is generated successfully"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/upload/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> addContratoFirmado(@RequestPart(value = "file") MultipartFile file, @PathVariable("id") Long id) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".addContratoFirmado ");
        try {
            CommandRequest request = new CommandRequest();
            request.setId(id);
            request.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED_MOBILES);
            request.setContract(file.getBytes());
            request.setUsername("NA");
            CommandResponse response = contractCommand.execute(request);
            if (response.getStatus().equals(Status.CONTRACT_OK)) {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } catch (Exception e) {
            log.error("Error adding signed contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    //----------------------
    @Autowired
    @Qualifier(value = "parseContractCommandWithSabadellV1")
    private Command parseContractCommandWithSabadellV1;
    @Autowired
    private ContractSignService contractSignService;
    @RequestMapping(value = "/getContratoManual/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getContratoManual(@PathVariable("id") Long id) {
    
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getContratoManual --->> ");
        try {
            CommandRequest request = new CommandRequest();
            request.setId(id);
            request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
            request.setUsername("NA");
            CommandResponse parseResponse = parseContractCommandWithSabadellV1.execute(request);
//            CommandResponse response = contractCommand.execute(request);
//            log.info("[tkn-service-contracts] :: " + new JSONObject(response).toString());
//            if (response.getStatus().equals(Status.CONTRACT_OK)) {
//            	 request = new CommandRequest();
//                 request.setId(id);
//                 request.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED);
//                 request.setUsername("NA");
//                  response = contractCommand.execute(request);
//                 if (response.getStatus().equals(Status.CONTRACT_OK)) {
//                     return new ResponseEntity<>(new JSONObject(response).toString(), HttpStatus.OK);
//                 } else {
//                     return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
//                 }  
//            } else {
                return new ResponseEntity<>(new JSONObject(parseResponse).toString(), HttpStatus.OK);
//            }         
            
        } catch (Exception e) {
            log.error("Error adding signed contract for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
    private static final String BASE64FINGER = "/6D/qAB6TklTVF9DT00gOQpQSVhfV0lEVEggMjgxClBJWF9IRUlHSFQgNDQ5ClBJWF9ERVBUSCA4ClBQSSA1MDAKTE9TU1kgMQpDT0xPUlNQQUNFIEdSQVkKQ09NUFJFU1NJT04gV1NRCldTUV9CSVRSQVRFIDAuNzUwMDAw/6gACkNvZ25heG9u/6QAOgkHAAky0yXNAArg8xmaAQpB7/GaAQuOJ2TNAAvheaMzAAku/1YAAQr5M9MzAQvyhyGaAAomd9oz/6UBhQIALAOBNgObDgOBNgObDgOBNgObDgOBNgObDgOTPwOwsgOJ8wOligN/+gOZkwODGgOdUgNv0gOGMAN5NgORdANzpAOKxQN6XgOS1wN6AgOSaAN4mAOQtwNzegOKkgN2owOOXQN1PAOMrgN5TwORkgODqgOd/wN/1wOZaAObzgO69wN+WQOXngOZgQO4NAOlRAPGUgO9WAPjNgOtVAPP/gO4uwPdrQOH1gOjAQOgVgPAaAOUXgOyCwOmNQPHcwO21QPbZgPPeAP49gO+hQPkoAPavAIaPwODKgOdZQOD3QOePAOm3wPIPgOl5APHEQOPUwOr/QOiDwPCeQOkOwPFEwOpRwPLIgOxvAPVSQO27QPbgwPTWAP9nAPFEAPsegO8hAPiOAO/LQPlaQPYHgIZ7wPcNwIabQPeJAIaqAOsiwPPDQIZ+wIfLgPRUQP7LgInkgIvfAO1DgPZQwPU9AP/jAIbegIg+QInjwIveQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP+iABEA/wHBARkCQ8YENPIAAAD/pgCMAAAAAwQDBQkQExMSFgMAAAABs7Wxsra3ArC4A66vuboEBaqrrK27vL0GCAmWo6Wmp6ipvr/AwcPEBwoLk5SVl52en6GkwsXGx8rt7gwNkpiZmpygosjJzc7P0dLT6+wSIjCNjo+QkZvLzNDU19ja5fIPEBkdHy0uLzNBRkhVVoqM1dbc4eTm7/Hz/6MAAwD3e77/AHfz/n7vw93u9v4fuiPt/D2/f7Z7/h4fj2/h/wDP19/h8Pf+Pxf8+3z8Pf8AB4OHw7P+/B8Xh8H6fDwn+Xw/H4Pg/P8AF4H/APfFw+Dv/l8XDP8A54eH4/F+jh74/D9XD4eH8vg4T/McPw8Pf4fg3e77PzeDw+D4vN/L7/afD4uH4/3+33e37+wL+Pu9vu934f8A3+f/AO9vt9wQVyoQqIh9tQ4rubLmkwbc2WyCaLT92EkhPr/ecTBIr/AWer4HV/wrYVaob9+3EhJa/wCHUyBFuT7J506MQzbiCsJIdMsLGiNEczx19cuE+zkvw5cSYaYVioZIvcXSITk1sYT0hOHDoHAvrd6OrVwiDITg7FMJoNJUmKxRYJHHmv1YWNH29XXBoggigqulAZO8u6xTzKRQSeEaJBwQapCTNmdQHlwpDhpBvlJ0QVNoMwilLVdWN2wJr4L7XoihB/tJNF3jcQQt5byCJNxxBcIEIwouTBQHJpCc0KEORNDdFZIqSIQREQS8vLUTvRZoeHnBExZIKJe57MxRJKIcKhkl3ok5V9UUYZy9HylO0KsGTQowlgqp2h5epsMS+MBCCnT5KXwh0z0QTy4gkOinajophMuRkUEkyINaKYuitaiHpeQaJXItk5xwaAs5tZ8zsggUiSYuMnGKGiTwc6st4xwFZiFwNcqL8hJJ9+Zta3elw4d+6nUUgIusxmiLCIEVRuQWbAEE9yChBKKBuOZBIIKhzmCk9EEVQ0VCEiEc+L3OHdZjQqLPDY1likCaNENZogEhPQw0OjSGMiSqJ4ISSxkELOSXop2BE5suW0MWy0NEIgqezQUJr0wrJCLsnD2Kw3IQ8pAqQ6CWBQxBuKhEHKCRlEXQkqHBmKhb2AsaOIjBZ0TDoEwe4nJCCUQu4UwYl0u7MBRb3iQSfyHMuBUNCgxnOaLuQz5rIvQkhM+ZWSgu6gERa5mnGC9EEglRjFcFkQoi40cw5CuQglBSJo4sDBqCqLGL4RMT6NxDhBCHgxzCN2RqaPVAkRe+VgjCpORooWgOElmqYCbn+ZmolRYvNlHN5ms9qO9Jolz6OLE5JBnVMHluPi0bHRUxITtqxm/VsBo42ohNg9ptBdJ9ge60wCXz5LMsQ5JKpMnMWcRQ0W/GDg51vp0CiI7qzH/E0NF/Qid6IzHgQjMbnJpBkEJZilQkkEoJBNQ2gqhBKLQREPN4QSTKWoSQkCkUxgKYg1hJBqyblLYmk0fnv2miwBrlqxL34UZKHdZaujWNfTx7mDiCU2L6u1imklG0Ia+nW4TTUIIRhD4K5S72o846rPmKwNxEqHVz6tm2xGFlWHe58SiREdThUIM7lmvJBzkRcYyqUDQiM5xd1mT8CzIHgNy/0IX4keA75B33zkMrlWOB3e5RWGRzOnihxKWZFIKqEViCEXSYgqQYGUmsuhAxY1E12vDtkUdyaGzYKzWVZcw5N1RXDsrXZLbeT04sQq49Pz/LPR+md3wezjvypFdfj838eH7fLtr2ez7NnJVPOXs9n3fVMB9Vfu/Z65Ce2H3evjylC2FvH5ZNMRhyYhyVr59GKoebkiGhyHfdx6rEPk5itig86HqrnexCoaPbLM2xyc7hjmTTL7ynemuRoaHgR3o7xCK/yXeSou8cxNDvJUVxEQroaFCNCU5pkL7OkiE5VQ8rHTeXTuEnkExVkDmhQyuwiiCJxD4tERFMcz83o12eL5w6dW75XFtnEIdK/dq6Ob1/Iz5W0NkYeAsJTxp9HPjrSQ3YOmeOWtqpCS1hISvcG1ZMOGeWnn3bmM8ynDFFG3F0+kNTUsLSHTZfq6uoOk8aKkW5/XHGQUoBFUHbqRT0UM5iia8FXE44Yq6bS9yQiM7kk5jwZZj7z0JC94/5mh4EUgaHfNCsyY54BBzyIIK0dOu1CEQpRDz82LwboBo6ptjjtUGimiWCUbI0RnUNs19Dxff0312tFC9ubo2uuc9HWef0UkN0cfsvQ015fn4tfZyla9nNo7dMOOPxfFy/T4+pHLHov6GBjXh2dXHLdevXxbft+zEP1bMZwxiMFf6demeorHp06Y2zSNx124vlsnvt6fPtqkZccn69MB6ePtwBRE5LnBMcfZy4tCBR1bMSgVpCMFEHoFs1Vfi8hAw1WzLDCcyMYTvQISCQW+ZCuQdb6goog++fxk0PeRsEcT3TZnYc2x330UIt24YneUBNVtemDY5pqiZm+IIJuRxcJrdGSoQVQh5aWrgEEYo4fFkHUlB5ZKtYwRIglwt5B4edbOGTo3LFwTt6SGgxRO7G5/GnSaibYkkZruYO84Ah3IVD0dQNFdDUrls6/PVwggcsXsShjt0WaiBx1kEno4/4/Ro2YOaNe8CJtp8XJs1Nr1XJ4FZ5/Z2bChlYI2mChPl1VVsugqhwbYmGyFtOpHNi/K8Q8DCtjnUYp2EGZW/0XkQYduA9MMzn3nibA++SQv64Bo8HfUMUDLw3cLIFAjV1LOjig4Qbo8/IbjtjAHY4Sbq9O26GbdsxrEpQ2h1SLW5PNoh4dqtJajQw6crQ9sssqqRGKNmrVLCvPoh0hElrREQtvaDD01guI0fGtjTlW1DeHCTtzNpCCoiz3Ri/PUJJAtdAhHpCCJBbAFKrdHWE6VDitmEV5fP6sMudw7A46UJ1eQbnrWqZqYF56hp/XeVAi0EK2rl29vq0EvZ3RpXS5vvwqkjF9VRRsydgkDDO2Z41jJImHgLOoDlXHgizoQ8P3a2JeGPeOZf2GiPvFTjMd58vN2/Z9Xm2cBmeSft+7d2aMt5PBHk7Ozb4vVpnMXTBfD/tq7/q5MnNxTwMPr1/TybE8Z0oFvP/ANdSijXFg8Dp8m7saaQQhYiRUaPn1JQ8EHYIMasuXkRL3EYDAuRp+Xq1UdUUKkUf6L0Q4QizO7wNnFBTTIMVlBB2xSBRCeUkEMa6QjmQYpJYbetFKLqpUiGwroybPgibtmn0dHJ0bOmjyyRbdo9Xh6hretFWQ8ZE7PR8X7pmaGebIYdnk8vVyce3b0xRHt9Pl5vp+T4cPF5/q8oOY+XX93T8H+/q0fo9j9zZ9Ufn8H5j9Phnu+LR5Pz/AC/Ry90218nGsj3j+NA0K72uwQxZHf0a/u5QR0+X19zL0fP+3z7C5jy2OZtG36/r3G/Zxej1OrkssNn2q/k6tPgx3HNZ2w8ny9O7Z5vB4OXFUOCxPL7NU9ltnz7q0jFo1bP2+ryWw+nrwaGpV1OGj5Pm+jyfJyibiVMQsunD0NjQ3MZV0xfrvEQiNoObXO2HL1obwQgpiHehQTyCnDhQITm5MDTpZwooaMKlEztns6XBoTaVCmMH2+XEl0NV4waNOjdOj53m80mI7OnL7NvL182QjU6Dzf8AF8Xh264to2aBFywO3VeowthjZmzEuYNuTpF8zvF+toWnoY8CqpL7ua3da0ETX+5L30nKXdNTCBNHmLjO1nKNDFXVIrxdaBBCeZ10x4/ZxVgElLUupzjs5efZKCTr0DTYTGj6mvcKMseS1sIjX1ebZljQ8vP+zk2hXjX0abcRE+rR5/Xy4uLNiNby79XZ8no02cIyMMuk7txUGEriRjowxEmqdnCNK82ATJ1UTFDLvsnC4vZBC+uEDkvmGRDKb+KrAxaI0STZ+zm/VzGASn0pV1af2ekiLoqJoz6OUSHJBrlF2W7ofec2z4IMgQXc55mKLMe4zm4pb5YKkL+k/wCC7yWYh98hZzD547rzmV/RfJWYy+LoLjkJwiiHx5MnOXV3+3XAINI09vPYW+f18W0qiKf0eTrquX5O0NsqiTE8fsgJ+hfTpv66ulf28nr8ljlt5eP7fF2VxqZ09DcUnZ19Ferm3aoiuvn7fm0wjaYwkYaZZp1+fUnBZig5hNsxOCbKIsIUylJm2JfGxpN8Itu1QxRWpra9oJNMKxfNLyr5cFAxXKqVShW0OgQduEXFqI51g6uTsrigY1xnMPBzx3DcbjHAUDQrul3/ANamaLgfl5DajnAK4zu8fI6CpPPeqLk6frfBwYJ6/S9Dt0avq6bERjp+bx40RfLt7Q+W7ojL6OdBm2a/H5jOPT7P27fk63s2OvL67bQ2HL4+Pt0SDhbC/o1tLjsteiJsEXGB2RUPcq3Q6SjYzrMhDiWku81TxNDDNKVMEIDgomYygiLnDoPCOp6EpxMlKCIwonpgmRLJBVscAWYvt3WdJ3KtRYy/n5YlgiNuNDpZ+a+CFRnVE+5NiwfNjmOBg0cQX3lLJByCeAoGH7rlB0v71+JBUO+YRoYdHOnyIKok9jcr+rnxNEHlNJobaPRzWcKCEHmits07GgIOIeNaD6ZaidNR5XQ813aenmyN0GzTphTWuj08XThFUjOFqG1sufqYvIUCDcXsDSslRNQaVbCAYFWh3hkcRaHQY0IiyEPFaO4iCncmjmAqVhxUJOjZGCKtAITzFGezohzdA2FCKRc5BgVyVCEUHohCdK5qPDrM8NmKezIkFF1niHBRuXcIhwqILfSzx3jRJf1f/6YAlgEAAgACBQQEDwodGRATAAAAs7UBAgMEBbK2BgextwgJsLgKCwwNDg8QEWlqra6vuboSEx8gISOqrLu8FBUXGBkaGxwdHiIlJigpKisvMzU/Q0RFqb2+v8IWJCwtLjAxMjc7PT5AQlGgpKeoq8DBw8TGJzg6QUZJSldbYZ6hpsXHyjQ2OTxHSFVYXF1imp2ipcjJy8z/owADAfvEHO7DnBgsuQ1KFnfxLBqptu1ISFkjW92d+Nw3kgg0WnYyQ3MJCS7u7fOsEa77pN9cH7Tt7L6Xvppqyb8R+7HX3dn0T2nUsX7ruhTD3fx8vW7mL9vd9eA0Y3bv3RjWDHk+jT+XnveHfr/7/wA+y8433+vS6k4db0P2eTsvF6nlguz5+6Cxr5btTqls368VxdxncbX4DISzwPeH+0eQ5TwNXgU4wlgyEgoPCUciUkYJREIIcbipgNcQF5LsN4MQkl9Rvhvjf2CMaYMXxfUcaRuwMaYQ7d0KTiQ13a6MEmI0a9pLQi92l0koLLOoS0NdMVW+aQSUM2CmNq+mnov6DwpQs8bVq9eCzI4IMbt2Ngwzr23jBVm6w7uv6WwEa6V82/eLIuju7Qx/Hrv5+zeRiNI03/Vd7P8ATu7vp+jzq31830fV9Jjy/Vr5vJqeSOzt3/T9X8e4jd1fs/Z2wOMb8eXt+nuL/T5f4fV2deNGPJ1Y/V+ryt/4Y7G/mNIdd53eeFvreHy4QcPUaydevV23jEE3wkN9dcYvl2dyzfduxeGt4xuqb4uVGNGpCZoWGCpBsxgoQ8GMnhP+hk8DVgllktjY542MlkdrZhqSNMF2EZuN9QwXxBTF767t93DpqRoxhw3TDeMaamtNdEI7NcGLxfEB2mDSO7R013YhEl013LONRLwDG7XR0xDS7dnTTW8YzZTG/QbCYkxBDYNHjxo0atnYbDkP7z8t+BfQF5AYNhS8mwsqSySMOHYXFguW1S8oMX1YIL41ILxfSbjDC3uQSMXcFN24aCUaYdEi/ZdksbmukFmNd+pDq7A0KLtFoS5llOZzfVejLaveOF5SEoBmww0WrUyGjOIMhhgEZJIKJAYq5qWHZgdjYTYkJIG1KjtILDzHRXu1aryDBsOcO8Xq0KDQgtjVCEYbys4vIw2NAphlSSGVhhdN0IQihKaTdcFEACLze94IxkYm5RlpfQgMwq65kXWbrDkhJJtvVOB9N6GgfA87zY7w8LKHAwwSc16FQbN5ICqQTeqNTBtYIJCg2KFWUYclkG+KDQhK3kYcySL4NgWSSzeCjwI1YeioYfTR2MFd2+7Uo4o46u3RzdxOP3QNWmmNYeryee8Fh36diXxv82mrJdHTz3Pq+n/zfu6tWMa7tevz9s79NOrtjczhxp5oY393164nBA7juvBjf5sQY0hYx26s9XbAQ6TjDv1xD1ixfeQXUuTp164IdYNxiL6S4m8EIQLpDftvpQrvjQhN+L7Madle0lgqlgNilhyGWwQWxzu0/wCp6jBDwou0piHRzMEmNMNSHGmBDEXMtNdECCHLfu1uE4Kt9MDDv7MX36asrpvuxvMDeVxF9bwGZhhSHsxVi4jTG6pBI1S8MDCDdJJSMWJUJ0htrchgcXzUkZclsQbHnbX9o/5vKwsm101yGG2gMEMMMYrvG7Ald1TfBAwQT3UN16ss460jt8++Blljdrdxf6NDXdXBjFyTt7cXdZHu0MSYhEhXzN28sEmJYuoya6yk4IIUppRWcLgaJN+y8uy9L75xW8jwEMXnGIapAF6F7IwYyxI1ScZFWocLVyAyDY5vCEP9LyEjYsl9ha+oEskkkaKEJCVLy1YcJJIJbQwQa6ahDDcWFi+NNbtMYu1xfW6VGGCHIjBBKYCcVEkRswUXYyJZYSi5MFGhIwUIYasEjQWQw1IAJIIaErqVGCiGxLEssrkXhq32YLPK9DQZHIkmwyNqQMEObCcRRkhyWEhhvLQGUlrejq1vLRghgJdOBo6yuVyVZIKMEFb1LITiVzGxDsw0OFqmw7xZ6Jx9ogfAy+sIwlEoOV4XYJQ2EmZmy1fXEqNSWxJxHevVg8T3jneiMeZdhmvO2M3IgGptW8sEC1YCChBYhJLkoUWSADYyQpKmTQw2JJCpBwAULGxlkl5goUc0WMPABYCzBZ4HwPRBnMnxvO7WHjcklzbPAQc7Y4SHM4k5nkMj1z3X8T/Z279/G6f/AH9/+mNHg7fJ/r9n2X/d5cZN47f8f4fy/l/LTffIl3dXd/r9fZjFiGE3/Ru+u4uabu3z+fBDUIYN/V/+/l3GYq46/wDP7F4EOv6fq0xRl00Vg3+RCEoQrON/dAjJ23au4l0ZY1JJxQxXSrjXcQQ0IuUb6RclrpRnTy7sAU0gljEfv7IxuvQoR27uvyfX+1vrS6pf6o7v0/v3/s7NKHVfW7+7/D/v/wCP/g9xQu6/q8/8u3r/AMvs3/s7ihr5f8tf9PPv+z9Ovn/a5dXZ/GP19f8AD6N3mNmt/wDD9N/9fLxY7n6+7c8RB4XlOvq7Q837d+w8v+OncR5r9vcZPl/9d2Ka+XfrYe7GO7B3eT9/V1FGnm+v6t3b2/p832eepA/T9fXvj/H9XZc1ZdZ7P19fnL/r8mjBOC8b9/2XMefu7YWm6Lm/t8j5MaFTDeQ0g0vDZwFHdubldL3oHWVxRgtrkzhoLffFxrpASru0GSmrbTTq1LN4YY0nfcvhpiL6btMRc7NdWmCN/m+vt8t4v2wddTSPP9P1SR3Rr2WwJ2w9jHZ571vi941deq/n/ZrliNWG+Or6dNhjA9n1bjgYvN9fw4cQ8LdkNrKTjUzKaOGLuxIxo3GGzLOLuIMr2MFziYCWzRLpBiHakaYcwLb7sDkNdyFCxVhqSkMk4aLIpKxc41ca2JCg3buCxljdDeEolLh2pDi2tdddA1W2mmIY7OpdFs6mlO6cXMg1byO11vCmNOHe3390PFobh42HwLY4Ao3dhLQIwWcYhkF0ciGGDUYWgVL4uGIKM3CCCblSVJxghzIYYDEBUgkMa3hyMg1veHIGcQQ5I5OwsRiByZSAvqFCl2zuShI2YYTLBDjXW8BAttLr2JTW9EdcQYlIxZi5OL0c2gwRiHNwMAwGdwqPCjOvE0T8oFQz0IukMDbHZ5erTBKGlb+fs/X5Lsjjtq6d27y3nG7rjssx2616v29WtG5ppBF4e76dLX7t3Usa73sYHEJ3G/B2dQ1ZbuhiVom8pd0N15QgWgwRe951qStSVScYuyw0CGNd8qUVkY1TdDF6IpIOuiThJ1a3jUnGsYDWcNGhp19XkhZcatCMXIcQGLChOIuwWcYgauZegvAwSfIJkZmpipDk7tYSpJUBGwlSMbsBBCxq1wksEkbihA1SmpTGHTEuII1vggvpGu7BARcWXtYDFd1CSLwQLjQdGTEtDAxpiCRl2tCHIxCtMUYWGLpDN0J0qJZhsYL6XZYaYISGNCl2qYNYb1TGmt6u0jBLCZt2oQOxEo8qPGP+12lCGTIcyGuG8Ni966RrAJBGNbY3Eiwm7EkaY1apG/RpiNCWdXTUgG+sIF9yiSN4vJjXRqTgxDiMYotRyLMFNLwtTIZOFR2FEGBkgqXyQYFklIM7u/CFBbFwhJbYkyFsjmGZZhzGwbWh+Y5jYZrmQZCZtypro5uemAoLRpvNrDdk10YZAh01Ive+t8DBOL790GENdMEhIUYviCjGmL0FsyQXqtrsDZch2lQokDN7EiZMlCWQKFmgwVYQ2GKpZoWYSCpBzud+cMn/AJLysEPGMt8cTR2BCXwQkNSRI1eJSB4CGEWhwq0KsskMXzLtAMOwgoYqQVMm8gcBrwFAviWWTK5k0arDRgpcaYkcRjhbwN4aY1SAhwQ4tq0xvMDjK4QzuhhcmjJh2pQHhvvjV4zBhP8AYnEUL8Fyxq5hJRxBmywIMFGCMMPEQ1KlVM1goQwFMQEFGAgloQUw0YUHJaoQEEELQJNrmbSCWcXhyLwyS2KXqVNjJeRm7QopLLeyQwVOMhGHYyQyc7mra8PEYDiJwPxPMJyMrxsEHEsPM375sbJtec2JmQ5vO5ubtfUO8tSTa7WzUsVMiCg2aDe1yhUMmWhRzKpy45mGXhYYP7jiauwKsMOSyXYIIMmAghWSpCtCWCpZMjJq8DJBLsCxC5uS0D2F2mRtbEpV2EFQ2MrVq0KrDiyFCt2qLk7QkqZlGUubGEU48MEHyOTmQwUC+Zk4HIGhDLLCwtQbJtu1IYakO1YLuaSywNmiReEhqFSWWFeJ4bnKbCzUzIFIcwkckvILmRepKwMEENCDYFmztfUDiIDlPzHMGw9JIGyBZMy+YuRRpiFgowrXDwN8SkmIWFlKjLVlJOJgW8m0DgIJTiNrUk2EYzJIJBlzS/KNQGWjmHAZX5x7zYeBo8rD8xBzrmw87UOZyeZ2AZmRtTjYJfuHqHGSZvqkPE0OEyvRheNed2H3j5T4ja5vMQ7GpU4zJ5WhzO1qy/AZu0sQes+E4StyzwGQ+09575L0gREbHpAUg+V6Ih/qSTxvsp4mz6j8BB8qfnYftEMp7p6jtfRfE/cPC/lfdKHxnuH4nmfYaPtND4X+0+Fl5zN4TvvGch+I5n4zgSUaHjGxzCJJmWfReFo8Tm+sMjwEJBxieA9E4hJfyN6IibT1j0RKMJBsWjzNX0UhEeEqMPfON2FGE2EFmDvvqlSXhe+wcptJZavGQ8hCQZD3n5GCCWWr4CiScpsPEeqsEFijxnAVYfSM3vuZxlGHkXoZznfVMzgONKnfPC959As+w8KVB4mH8zk+67XhJPG+ylGj+R+UHiORo/jaFCCzR9co1OJ2nC/0HpPC7HkPukPuJ7L3iB5E5zkfaPG86Q+J9t959Y9gfcfgPwPzkvoEL/MylX8hLzmS/AVYOc4zwE4h8J4UeY2npHCw95k8TyMELyvgfbPvvqub9t5zIyfGf3Nk919o/G9EanQ3HqtU6IUejzfhPyPzn/EeiVekCDv/owADAekC0zpAiY/qOhxPWOhxPRfzOPiOVudEQcR+NPzm0g+Q/A8hL8D4DogiX8p959I/3HES/E8JLDsKPpPidp7D6ZQow8D4CHjYIIOc8RBtKJQPAeww0EavxoIy+J2nebEGwo/hKPxm1asHssY9lhhglIX33kMmX1GE2nPhIPTeRYZKLR4UgvDDBZ2kPecnkvJQq1e+7RgKHokFjIk9Jh+0ZNWGgbSVo2fSZKEpVqbDawhZs0eAkzfZDM75A+Fhk4CAOF2lnNzKliwQ1OUcznaElUaEFTjb1WGr42XmwkPG0CCzJVs1RbDsKDBAjDBLQ7xkiMMJyJCGZLJylCoJQ5mGDYfbCCzwpUoZtBk9FJbPANiymbBzkkpL6JBZSHndqQImw2kvE7HvkHpkBRkk9gokuRxocpQh9N9Bo+NzYKrwBJLIewbX1GH4D0Eh+FDiUhheU9lk5Sr/AL1X3yVgCSHhWT75BLxMkvwnOmT4yCxzMv4HxvqtH0yx0M5BY5SDocSD0npAUc9Z+U+c/wCB/W/K9ICMvRqvSEew/MPSClg/MnzJ0gmodIk4PSArJ0gV+dIDxn3T1njekBBXokD8p/WdDu9EW/EeI8T0cL0T78p/ef3HSBhLznwtnojF+Qo9GeQ9Ge9JPIr8hJ8ywXICFk/EZKwH42wS2PwtAOAoe+0cmHaskMh7wQ0AqqywfjAAowLQg9x2mxV+4Q5lBYVs5lTwNSi1DYQVaEPtslHY5AyUffXvsHjSjtaEG1kko+Mho8bVfG8Zk0aPePbbABK2YZZPVPSWhYglsfdILMksniOd4lkobX32CSi2DxtnhMko1cn8TkQ8L3ngOd4zYe8EPuNTjeV+Eq84cj6bzpm5vA/gAh9x9Fk8R7wEOT944xILPC+02CpABDJB6xk8htdpxnvpJk+As+osLLJ/O2XY+sc7sShDBB4j2QseEIOV2stCDa98PhfCchVlq7GGSH8LUg8L6bkvEZn3GivhaGRzGS8LwEHIeB8Z6765909t8D6pwPzh4mXhcyD7pBD+cgg8A+J8Z4Cz8hmfzlgKhJ3z3g+EzeJlNhJ8ivrmx5Xwj8SvrAwnsnqn6HlKvKcRBIQd49B2h7h4GQ4j74Su1yJeQ9N9BksH8xUChxrzvIfadjJC+25MrQOQhYeM4mqQwZBC1OAghl9w2vC0KjzHC985WCA+2vCqtD3jYQ2AgqtFq0IOUufaJWytlcjvgXOJlgoZBKycB3ihkEtgLLQoQ+y0WWChyhsZPCy5PMskh40oySbWVs2XJ+2FA2KyB9soWJCHJsELV9tqFTMgzKPiJKsEK8xtHwNmhUAoQsK2fEZELIVV4mQ9tkqFgJMiT28ZmxYeBg95sbAkKr7pBksAWfkVk2sB7pRbNnYCv3ghWGoAFD8F4xhZVllaPxhmwSv9BRzf6Q6QdPekDynkeijejHejHPSf+z0gIo9EY9F4dEkfoehqPC9IFxnSAuB+deiJKPzHypeHpA6F6Gw6QFXfnOkBDXpBGgOkD22j0iN+9EUvzgf2FnpAZ5eiOekBnnpIL3//oQ==";
    @ApiOperation(value = "verifi ine by xapi")
	@RequestMapping(value = "/testContract", method = RequestMethod.POST)
	public ResponseEntity<String> testContract(@RequestBody String req) {
		log.info("[tkn-service-contracts] :: " + this.getClass().getName() + ".getContratoManual --->> ");
		try {

			JSONObject json = new JSONObject(req);
			CommandRequest request = new CommandRequest();
			request.setId(json.optLong("id"));
			request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
			request.setUsername("NA");
			CommandResponse parseResponse = parseContractCommandWithSabadellV1.execute(request);
			SignContractRequestDTO signContractRequestDTO = new SignContractRequestDTO();
			signContractRequestDTO.setBase64Finger(BASE64FINGER);
			signContractRequestDTO.setContentType("wsq");
			signContractRequestDTO.setOperationId(request.getId());
			signContractRequestDTO.setHash(request.getScanId());
			signContractRequestDTO.setContractSigned(parseResponse.getContract());
			try {
				byte[] contractSigned = contractSignService.signContract(signContractRequestDTO);

				json.put("ResponseHuellas", contractSigned);
			} catch (Exception e) {
				// TODO: handle exception
			}
//			json.put("ResponseInfo", parseResponse);
			return new ResponseEntity<>(json.toString(), HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error adding signed contract for: {} with message: {}", e.getMessage());
			return new ResponseEntity<>("50005", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
    
    // print fingers 
    private void method() {
    	
    }
    
    //----------------------

    
    @ApiOperation(value = "Uploads contract signed and stores in the document manager")
    @RequestMapping(value = "/demo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getDemo(@RequestBody ContractDemoDTO dto) 
    {
    	//log.info("lblancas: "+this.getClass().getName()+".{getContractDemo() }"+dto.toString());
    	log.info("Valores "+  dto.toString());
    	byte[] responseEntity =null;
    	try
    	{
    		log.info("Intenta con  : "+dto.isConhuella());
//    		responseEntity =getContractDemoLocaly(dto.getNombre(),dto.getCurp(),dto.getDomicilio(),dto.isConhuella());
    		responseEntity =getContractDemoLocaly2(dto.getNombre(),dto.getCurp(),dto.getDomicilio(),dto.isConhuella());    		
    	}catch(Exception e)
    	{
    		log.info("Intenta sin imagen");
//    		responseEntity =getContractDemoLocaly(dto.getNombre(),dto.getCurp(),dto.getDomicilio(),false);
    		responseEntity =getContractDemoLocaly2(dto.getNombre(),dto.getCurp(),dto.getDomicilio(),false);
    	}
 
    	return new ResponseEntity<>(Base64Utils.encodeToString( responseEntity) , HttpStatus.OK); 
    }
    
    //TODO
    /**
     * Genera Contrato Amigo
     * @param nombre
     * @param curp
     * @param domicilio
     * @param conImagen
     * @author AJGD
     * @return
     */
	private byte[] getContractDemoLocaly2(String nombre, String curp, String domicilio, boolean conImagen) {
		log.info("************** INICIANDO CONTRATO AMIGO *****************");
		try {
			// Llenando datos..
			List<ContractItemDto> dataList = new ArrayList<>();
			ContractItemDto data = new ContractItemDto();
			data.setNombre("nombre");
			data.setValor(nombre);
			dataList.add(data);
			if (conImagen) {
				try {
					data = new ContractItemDto();
					log.info("Estamos tomando la imagen : /home/Huella.jpg");
					data.setNombre("huella");
					data.setValor(Base64.getEncoder().encodeToString(Image.getInstance("/home/Huella.jpg").getOriginalData()));
					dataList.add(data);
				} catch (Exception e) {
					log.info("ERROR: error al encontrar la imagen.");
				}
			}
			RequestContractDto dto = new RequestContractDto();
			dto.setCodeContract("NDA");
			dto.setCampos(dataList);
			// Creando Pdf..
			String contractBase64 = new ContractService().getContract(dto);
			byte[] resByte = Base64.getDecoder().decode(contractBase64);
			// Firmando en karalundi y guardado en tas..
			BidClie bidClie = new BidClie();
			bidClie.setRfc(curp);
			bidClie.setNomClie(nombre);
			bidClie.setApePate(nombre);
			bidClie.setApeMate(nombre);
			String email = nombre;
			if(conImagen) {
				CommandResponse res = karalundiSignService.firmadoKaralundi(51843L, contractBase64, bidClie, email);
				resByte = res.getSignedContract();
			}
			return resByte;
		} catch (Exception e) {
			// Aqui va el error..
			log.error("Error" + e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
    
    /** 
     * Esto es un ejemplo
     * @param nombre
     * @param curp
     * @param domicilio
     * @param huella     * 
     * @return
     */ 
    private byte[] getContractDemoLocaly(String NOMBRE, String CURP, String DOMICILIO,boolean conImagen) {
		 ByteArrayOutputStream stream=null;
 /*
		 byte[] huellaUTF8 =null;
		 if(conImagen) 
			 huellaUTF8 = encodeBase64(getImagen("/home/huellaC.jpg"));
	*/ 
		 try 
		 {
			//log.info("Crea documento en memoria");
		   Document document = new Document(PageSize.A4, 35, 30, 50, 50);
				 
  		   stream = new ByteArrayOutputStream();
  		   //String pdfNewFile="/home/demo_"+(new Date()).getTime()+".pdf";
  		   //PdfWriter.getInstance(document, new FileOutputStream(pdfNewFile));
		   PdfWriter.getInstance(document, stream);
		    
		   // Abrir el documento
		   document.open();
		    
		   Image image = null;
		 
		   //log.info("Crear las fuentes para el contenido y los titulos");
		   // Crear las fuentes para el contenido y los titulos
		   Font fontContenido = FontFactory.getFont(
		     FontFactory.TIMES_ROMAN.toString(), 11, Font.NORMAL,
		     BaseColor.DARK_GRAY);
		   
		   Font fontSubrayado = FontFactory.getFont(
				     FontFactory.TIMES_ROMAN.toString(), 11, Font.UNDERLINE,
				     BaseColor.DARK_GRAY);	
		   
		   Font fontTitulos = FontFactory.getFont(
		     FontFactory.TIMES_BOLDITALIC, 11, Font.UNDERLINE,
		     BaseColor.BLACK);
		   // Creacion del parrafo
		   Paragraph paragraph0 = new Paragraph();
		 
		   // Agregar un titulo con su respectiva fuente
		   paragraph0.add(new Phrase("CONTRATO DE AMISTAD", fontTitulos));
		 
		   //log.info("Agregar saltos de linea");
		   // Agregar saltos de linea
		   paragraph0.add(new Phrase(Chunk.NEWLINE));
		   paragraph0.add(new Phrase(Chunk.NEWLINE));
		   paragraph0.setAlignment(Element.ALIGN_CENTER);
		   document.add(paragraph0);
		   
		   Date d = new Date();
           Calendar c = new GregorianCalendar(); 
           c.setTime(d);
		   
		   String dia, mes, anio;

           dia = Integer.toString(c.get(Calendar.DATE));
           mes = Integer.toString(c.get(Calendar.MONTH)+1);
           anio = Integer.toString(c.get(Calendar.YEAR));
           
		   Paragraph paragraph = new Paragraph();
		   paragraph.add(new Phrase("Ciudad de México de "+dia +" de "+ MONTH(mes) + " de " +anio , fontTitulos));
			
		   // Agregar saltos de linea
		   paragraph.add(new Phrase(Chunk.NEWLINE));
		   paragraph.add(new Phrase(Chunk.NEWLINE));
		    
		   // Agregar contenido con su respectiva fuente
		   paragraph.add(new Phrase("CONTRATO ENTRE “ ",fontContenido));
		 
		   paragraph.add(new Phrase(NOMBRE,fontSubrayado));
		   
		   paragraph.add(new Phrase(" ” DE AHORA EN ADELANTE “EL MEJOR AMIGO” Y BURÓ DE IDENTIDAD DIGITAL DE AHORA EN ADELANTE “BID” "
		   		+ "PARA CELEBRAR EL INICIO DE UNA AMISTAD LARGA Y DURADERA SIN FINES DE LUCRO.",
			       fontContenido));
		   
		   //log.info(" DE AHORA EN ADELANTE,....");
		  
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase("EL MEJOR AMIGO CON CURP: ", fontContenido));
		   paragraph.add(new Phrase( CURP , fontSubrayado));
		   paragraph.add(new Phrase("  Y CON DOMICILIO CONOCIDO EN:  " , fontContenido));
		   paragraph.add(new Phrase(DOMICILIO , fontSubrayado));
		   paragraph.add(new Phrase( " ,  ACEPTA QUE LOS DATOS ANTES MENCIONADOS SON CORRECTOS Y QUE HAN SIDO EXTRAÍDOS DE MANERA AUTOMÁTICA "
		   		+ "MEDIANTE UN PROCESO DE EXTRACCIÓN DE DATOS “OCR” AGILIZANDO EL PROCESO PRESENTADO POR BID DURANTE LA DEMOSTRACIÓN.", fontContenido));
		   
		   paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
		   document.add(paragraph);
		   paragraph=new Paragraph();
		   
		   paragraph.add(new Phrase(Chunk.NEWLINE));  
		   
		   //log.info(" SERVICIOS INTEGRALES OFRECIDOS POR BID,....");
		   paragraph.add(new Phrase("SERVICIOS INTEGRALES OFRECIDOS POR BID", fontTitulos));
 
		   paragraph.setAlignment(Element.ALIGN_LEFT);
		   document.add(paragraph);
		   paragraph=new Paragraph();
		   
		   paragraph.add(new Phrase("INTEGRAMOS LAS MEJORES TECNOLOGÍAS DISPONIBLES OFRECIENDO UN PROCESO END TO END (E2E) PARA CREAR UNA VERDADERA"
		   		+ " EXPERIENCIA DIGITAL PARA SU CLIENTE Y FACILITAR UN ENROLAMIENTO RÁPIDO Y SEGURO, DANDO ADEMÁS LA SEGURIDAD Y CERTEZA DE QUE ES "
		   		+ "QUIEN DICE SER GRACIAS A LA IMPLEMENTACIÓN DE TECNOLOGÍAS DE VALIDACIÓN BIOMÉTRICA Y TESTIFICACIÓN DE DOCUMENTOS ANTE DIVERSAS ENTIDADES.", fontContenido));
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
		   document.add(paragraph);
		   
		   paragraph=new Paragraph();
		   
		   //log.info(" ENROLAMIENTO BIOMÉTRICO DIGITAL,....");
		   paragraph.add(new Phrase("ENROLAMIENTO BIOMÉTRICO DIGITAL", fontTitulos));
		   paragraph.setAlignment(Element.ALIGN_LEFT);
		   document.add(paragraph);
		   paragraph=new Paragraph();  
		   paragraph.add(new Phrase("A CUALQUIER HORA Y DESDE CUALQUIER LUGAR, ALGUIEN PUEDE SER SU NUEVO CLIENTE.", fontContenido));
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase(Chunk.NEWLINE));	
		   paragraph.add(new Phrase("CREAMOS UNA VERDADERA EXPERIENCIA DIGITAL PARA SUS CLIENTES, REDISEÑANDO EL PROCESO DE ENROLAMIENTO DE NUEVOS CLIENTES, IMPLEMENTADO UN PROCESO E2E, SIN PAPEL, RÁPIDO Y SUMAMENTE SEGURO.", fontContenido));
		   paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
		   document.add(paragraph);
		   paragraph=new Paragraph();
		   paragraph.add(new Phrase(Chunk.NEWLINE));
		   
		   paragraph.add(new Phrase("REGISTRO Y VALIDACIÓN BIOMÉTRICA", fontTitulos));
		   paragraph.setAlignment(Element.ALIGN_LEFT);
		   document.add(paragraph);
		   paragraph=new Paragraph();  
		   paragraph.add(new Phrase("CONOZCO A MI CLIENTE, ES QUIEN DICE SER", fontContenido));
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   paragraph.add(new Phrase("EL MECANISMO MÁS EFICIENTE PARA IDENTIFICAR A SU CLIENTE, PROVEEDOR O EMPLEADO ES LA IMPLEMENTACIÓN DE REGISTRO Y VALIDACIÓN "
		   		+ " BIOMÉTRICA DENTRO DE LOS PROCESOS DE NEGOCIO QUE REQUIERAN LA IDENTIFICACIÓN DE QUIENES INTERACTÚAN EN ESA TRANSACCIÓN.", fontContenido));
		   paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
		   document.add(paragraph);
		   paragraph=new Paragraph();
		   paragraph.add(new Phrase(Chunk.NEWLINE));
		   paragraph.add(new Phrase(Chunk.NEWLINE)); 
		   document.add(paragraph);
		    
		   //log.info(" Tabla PDF....");  
		   PdfPTable table = new PdfPTable(5);
		   float dimension[]={80,80,40,30,80};
		   table.setTotalWidth(dimension);
		   // Agregar la imagen anterior a una celda de la tabla
		   PdfPCell cel10 = new PdfPCell(new Phrase(" "));
		   cel10.setFixedHeight(50);
		   cel10.setBorder(Rectangle.NO_BORDER);
		   cel10.setColspan(1);
	       table.addCell(cel10);
	       
	       
	       Paragraph FIRMA = new Paragraph();
	       FIRMA.add(new Phrase(NOMBRE, fontTitulos));
		   FIRMA.add(new Phrase(Chunk.NEWLINE));  	
		   FIRMA.add(new Phrase(Chunk.NEWLINE));
		   FIRMA.add(new Phrase("EL MEJOR AMIGO", fontContenido));
		   FIRMA.setAlignment(Element.ALIGN_CENTER);
		   PdfPCell cel20 = new PdfPCell(FIRMA);
		   cel20.setColspan(2);
		   cel20.setBorder(Rectangle.NO_BORDER);
		   cel20.setFixedHeight(50);
	       table.addCell(cel20);
	       
	       if(conImagen)
	       {
	    	   try
	    	   {
			       PdfPCell cel30 =  null; 
			       log.info("Estamos tomando la imagen : /home/huellaC.jpg" );
		    	   image = Image.getInstance("/home/huellaC.jpg");
		    	   image.scaleAbsolute(50f, 50f);
		    	   cel30= new PdfPCell(image); 
			       cel30.setBorder(Rectangle.NO_BORDER);
			       cel30.setColspan(1);
			       cel30.setFixedHeight(50);
			       table.addCell(cel30);  
	    	   }
	    	   catch(Exception e)
	    	   {
	    		   PdfPCell cel30 =  null; 
			       log.info("Estamos tomando la imagen : \\home\\huellaC.jpg" );
		    	   image = Image.getInstance("\\home\\huellaC.jpg");
		    	   image.scaleAbsolute(50f, 50f);
		    	   cel30= new PdfPCell(image); 
			       cel30.setBorder(Rectangle.NO_BORDER);
			       cel30.setColspan(1);
			       cel30.setFixedHeight(50);
			       table.addCell(cel30);
	    	   }
	       }
	       else
	       {
	    	   PdfPCell cel30 = new PdfPCell(new Phrase(" "));
		       cel30.setBorder(Rectangle.NO_BORDER);
		       cel30.setColspan(1);
		       cel30.setFixedHeight(50);
		       table.addCell(cel30); 
	       }
	       PdfPCell cel40 = new PdfPCell(new Phrase(" "));
	       cel40.setFixedHeight(50);
	       cel40.setBorder(Rectangle.NO_BORDER);
	       cel40.setColspan(1);
	       table.addCell(cel40);  
	       //log.info(" Crea tabla "); 
	       
		   document.add(table); 
		   // Cerrar el documento
		   document.close();
		   //log.info(" Cierro doc "); 
		   // Abrir el archivo
		   //File file = new File(fileName);
		   //Desktop.getDesktop().open();
		  } catch (Exception ex) {
		   ex.printStackTrace();
		  }
		 log.info(" fin "); 
		 return stream.toByteArray();
	}
    /**
     * Obtiene mes depende del numero de mes
     * @param mes
     * @return
     */
    private static String MONTH(String mes) 
	{
		if(mes.equals("01")) mes = "Enero";
		if(mes.equals("02"))mes = "Ferbero";
		if(mes.equals("03"))mes = "Marzo";
		if(mes.equals("04"))mes = "Abril";
		if(mes.equals("05"))mes = "Mayo";
		if(mes.equals("06"))mes = "Junio";
		if(mes.equals("07"))mes = "Julio";
		if(mes.equals("08"))mes = "Agosto";
		if(mes.equals("09"))mes = "Septiembre";
		if(mes.equals("10"))mes = "Octubre";
		if(mes.equals("11"))mes = "Noviembre";
		if(mes.equals("12"))mes = "Diciembre";
		return mes;
	}
    /**
     * Codifica a Base 64
     * @param encodedBytes
     * @return
     */
    private byte[] encodeBase64(byte[]  encodedBytes)
	{
		return  Base64.getEncoder().encode(encodedBytes);
	}
	/**
	 * Service para decodificar Base 64.
	 * @param encodedBytes
	 * @return
	 */
	private byte[] decodeBase64(byte[]  arrayByte )
	{
		return  Base64.getDecoder().decode(arrayByte);
	}
	private byte[] getImagen(String fileHuella)
	{	
		byte contenido[]=null;
		try {
			File fichero = new java.io.File(fileHuella); 
			FileInputStream ficheroStream = new FileInputStream(fichero); 
			contenido = new byte[(int)fichero.length()]; 
			ficheroStream.read(contenido);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contenido;
	}
	
}
