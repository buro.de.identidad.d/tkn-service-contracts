package com.teknei.bid.controller.rest;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.contracts.dto.ContractItemDto;
import com.contracts.dto.RequestContractDto;
import com.contracts.service.ContractService;
import com.google.gson.Gson;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.RequestType;
import com.teknei.bid.command.Status;
import com.teknei.bid.controller.rest.crypto.Decrypt;
import com.teknei.bid.dto.AcceptancePerCustomerDTO;
import com.teknei.bid.dto.ContratoTmsDTO;
import com.teknei.bid.dto.SaveDocTmsDTO;
import com.teknei.bid.dto.TMSDocumentosDTO;
import com.teknei.bid.service.TMSFideicomizo;
import com.teknei.bid.persistence.entities.TMSCuestionarioPP;
import com.teknei.bid.persistence.repository.BidTMSCuestionarioPPRepository;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;

@RestController
@RequestMapping(value = "/contractTMS")
public class ContractControllerTMS {
	@Autowired
	private BidTMSCuestionarioPPRepository bidTMSRepository;
	@Autowired
    @Qualifier("contractCommandTMS")
    private Command contractCommandTMS;
	@Autowired
    private Decrypt decrypt;
	
	private static final Logger log = LoggerFactory.getLogger(ContractControllerTMS.class);
	private static final String RECURSOS = "/home/PDF_RECURSOS.pdf";
	private static final String INMUEBLES = "/home/PDF_INMUEBLES.pdf";
	
	@RequestMapping(value = "/saveContracts/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> getSaveContractTMS(@PathVariable("id") Long id, @RequestBody SaveDocTmsDTO saveDoc) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getUnsignedContract ");
    	
		ArrayList<byte[]> arrayDocmentos = new ArrayList<byte[]>();
		arrayDocmentos = saveDoc.getArrayDoc();
		log.info("Save documente TAS in true whit size of ::::::: " +arrayDocmentos.size());
		 CommandResponse response = new CommandResponse();
		//byte [] unsignedContract = null;
    	//byte [] signedContract = null;
    	
        if(saveDoc.isFirma()) {
    			
        	try {
        		for( byte [] signedContract : arrayDocmentos ){
        			
                CommandRequest request = new CommandRequest();
              
                request.setRequestType(RequestType.CONTRACT_REQUEST_SIGNED);
                request.setId(id);
                request.setSignedContract(signedContract);
                
                response = contractCommandTMS.execute(request);
        		}
                
                if (response.getStatus().equals(Status.CONTRACT_OK)) {
                    //contract = response.getContract();
                    return new ResponseEntity<>(HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
                }
        		
            } catch (Exception e) {
                log.error("Error generating contract in ContractControllerTMS for: {} with message: {}", id, e.getMessage());
                e.fillInStackTrace();
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }
        	
        	
        }else {
        		
        	try {
        		for( byte [] unsignedContract : arrayDocmentos ){
        		
                CommandRequest request = new CommandRequest();
              
                request.setRequestType(RequestType.CONTRACT_REQUEST_UNSIGNED);
                request.setId(id);
                request.setContract(unsignedContract);
                
                response = contractCommandTMS.execute(request);
        		}
                
                if (response.getStatus().equals(Status.CONTRACT_OK)) {
                    //contract = response.getContract();
                    return new ResponseEntity<>(HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
                }
        		
            } catch (Exception e) {
                log.error("Error generating contract in ContractControllerTMS for: {} with message: {}", id, e.getMessage());
                e.fillInStackTrace();
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }	
        	
        }

    }
	
	 @RequestMapping(value = "/PDFRecursos", method = RequestMethod.POST)
	    public ResponseEntity<String> getPDFRecursos(@RequestBody TMSDocumentosDTO dto){
		 
		 log.info(" **************** RECURSOS TMS *************** ");
	    	log.info(" ---------------- Informacion de Recursos ------------------- " +dto.toString());
	    	
	    	byte[] savebyte = null;
	    	try {
	    		
				if(dto.isConfirma()) {
					
					log.info(" Huella con: " + dto.isConfirma() + "Con Documento " +RECURSOS);
					
					savebyte = getDocumentos(RECURSOS, dto.getIndexLeft());
					
				}else {
					
					File file = new File(RECURSOS);
					savebyte = Files.readAllBytes(file.toPath());
				}
				
				
			}catch(Exception e) {
				
				e.fillInStackTrace();
			}
	    	return new ResponseEntity<>(Base64Utils.encodeToString( savebyte ), HttpStatus.OK); 
	    	
	    }
	    
	    public static byte[] getByte(File file) throws FileNotFoundException, IOException{
	    	
			byte[] bytePDF = new byte[1024];
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			FileInputStream fis = new FileInputStream(file);
			int read;
			
			while((read = fis.read(bytePDF)) != 1) {
				os.write(bytePDF, 0, read);
			}
			
			fis.close();
			os.close();
			return os.toByteArray();
		}
	    
	    String huellaTemp;
	    
	    @RequestMapping(value = "/PDFInmuebles", method = RequestMethod.POST)
	    public ResponseEntity<String> getPDFInmuebles(@RequestBody TMSDocumentosDTO dto){
	    	
	    	log.info(" **************** INMUEBLES TMS *************** ");
	    	log.info(" ---------------- Informacion de Inmubles ------------------- " +dto.toString());
	    	
	    	byte[] savebyte = null;
	    	
	    	try {
	    		
	    		if(dto.isConfirma()) {
	    			
	    			log.info(" Huella con: " + dto.isConfirma() + "Con Documento " +INMUEBLES);
	    			savebyte = getDocumentos(INMUEBLES, dto.getIndexLeft());
	    			
	    		}else {
	    			
	    			File file = new File("/home/PDF_INMUEBLES.pdf");
	    			savebyte = Files.readAllBytes(file.toPath());
	    		}
				
			}catch(Exception e) {
				
				e.fillInStackTrace();
			}
	    	return new ResponseEntity<>(Base64Utils.encodeToString( savebyte ), HttpStatus.OK); 
	    	
	    }
	    
    /**
     * Firmado de documentos.
     * @param SRC
     * @param INDEX
     * @return
     */
	private byte[] getDocumentos(String SRC, String INDEX) {
		ByteArrayOutputStream stream = null;
		try {
			stream = new ByteArrayOutputStream();
			PdfReader pdfReader = new PdfReader(SRC);
			PdfStamper pdfStamper = new PdfStamper(pdfReader, stream);
			String huellaFinal = decrypt.decrypt(INDEX);
			huellaTemp = huellaFinal;
			byte[] imageByte;
			imageByte = Base64Utils.decodeFromString(huellaFinal);			
			File rutaHuella = new File("/home/fingerToFirma.png");
			if (rutaHuella.exists()) {
				rutaHuella.delete();
				rutaHuella = new File("/home/fingerToFirma.png");
			}
			
			InputStream in = new ByteArrayInputStream(imageByte);
			BufferedImage bImageFromConvert = ImageIO.read(in);
			ImageIO.write(bImageFromConvert, "png", rutaHuella);
			Image image = Image.getInstance("/home/fingerToFirma.png");
			for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
				PdfContentByte content = pdfStamper.getUnderContent(i);
				image.setAbsolutePosition(520f, 400f);
				image.scaleAbsolute(50f, 70f);
				content.addImage(image);
			}
			pdfStamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return stream.toByteArray();
	}
	
	
	
	    //TODO CAMBIOS BRANOM------------------->>>>>>>>>>>
	    
		private void createImageTemp(String b64) {
			try {
				String huellaFinal = decrypt.decrypt(b64);
				byte[] imageByte;
				imageByte = Base64Utils.decodeFromString(huellaFinal);
				File rutaHuella = new File("/home/fingerToFirma.png");	
				if (rutaHuella.exists()) {	
					rutaHuella.delete();
					rutaHuella = new File("/home/fingerToFirma.png");
				}
				InputStream in = new ByteArrayInputStream(imageByte);
				BufferedImage bImageFromConvert;	
				bImageFromConvert = ImageIO.read(in);	
				ImageIO.write(bImageFromConvert, "png", rutaHuella);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		}
		
		@Deprecated
		@ApiOperation(value = "Contrato Fideicomiso de TMSourcing")
		@RequestMapping(value = "/TMSFideicomiso", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
		public ResponseEntity<byte[]> getPDFFideicomiso(@RequestBody String dto) {
			log.info("Valores TMSFideicomiso ::::::::::::::: "+ dto.toString());
			TMSFideicomizo fideicomizo = new TMSFideicomizo();
			try {
				JSONObject fideicomisoDTO = new JSONObject(dto);
				String b64 = (String) fideicomisoDTO.optString("huellaIndex", "");
				if (!b64.isEmpty()) {
					createImageTemp(b64);
				}
				fideicomizo.setDataFideicomizo(dto);
				File file = new File("/home/ContraFideicomisoFinal.pdf");
				byte[] resByte = loadFile(file);
	
				return new ResponseEntity<>(resByte, HttpStatus.OK);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		@ApiOperation(value = "Contrato Fideicomiso de TMSourcing")
		@RequestMapping(value = "/TMSFideicomiso3", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
		public ResponseEntity<byte[]> getPDFFideicomiso3(@RequestBody RequestContractDto dto) {	
			try {
				//Guardado de formulario en BD..
				TMSCuestionarioPP n = new TMSCuestionarioPP();				
				n.setIdClie(Long.parseLong(dto.getIdTas()));
				n.setData(new Gson().toJson(dto));
				bidTMSRepository.save(n);
				//Creando Pdf..
				ContractService contractService = new ContractService();
				byte[] resByte = Base64.getDecoder().decode(contractService.getContract(dto));	
				return new ResponseEntity<>(resByte, HttpStatus.OK);
			} catch (Exception e) {
				//Aqui va el error..
				log.error("Error"+e.getMessage());
				e.printStackTrace();
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}		
		
		@ApiOperation(value = "Contrato Fideicomiso de TMSourcing")
		@RequestMapping(value = "/TMSFideicomiso2", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
		public ResponseEntity<byte[]> getPDFFideicomiso2(@RequestBody String dto) {
			//TODO GUARDAR EN bid.bid_tms
			
			log.info("Valores TMSFideicomiso ::::::::::::::: "+ dto.toString());
//			String huellaIndex = dto.toString();
//			String huellaIndexTemp = "";
//			if(huellaIndex.contains("huellaIndex")) {
//				huellaIndexTemp = huellaIndex.substring(huellaIndex.indexOf("huellaIndex")+16, huellaIndex.indexOf("patrimonial")-5);
//			}
			String validDto = dto;	
			validDto = validDto.replace("\\", "")
					.replace("\"{", "{").replace("\"[", "[")
					.replace("}\"", "}").replace("]\"", "]");
			log.info("validDto> "+ validDto);
			String[] fe = getDate().split("_");
			String day = fe[1];
			String month = fe[0].substring(0, 1).toUpperCase()+""+fe[0].substring(1,fe[0].length());
			String year = fe[2];
			ContractService contractService = new ContractService();
			
			try {
				JSONObject fideicomisoDTO = new JSONObject(validDto);
				RequestContractDto obj = new RequestContractDto();
				ArrayList<ContractItemDto> campos = new ArrayList<ContractItemDto>();
				ContractItemDto element = new ContractItemDto();
				String SELECT_MARCK = "X"; 
				
			
				//----------------------------//
				JSONObject patrimonial = (JSONObject) fideicomisoDTO.opt("patrimonial");
				//Guardado de formulario en BD
				TMSCuestionarioPP n = new TMSCuestionarioPP();				
				n.setIdClie(Long.parseLong(patrimonial.optString("operationId")));
				n.setData(validDto);
				bidTMSRepository.save(n);
				JSONObject datosGenerales = (JSONObject) patrimonial.opt("DatosGenerales");	
				log.info("> pag :"+ 1);//---------------------------------------->
				element.setNombre("contratoNo");
				element.setValor("001");
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("nombreTitulo");
				element.setValor(datosGenerales.optString("nombres")+" "+datosGenerales.optString("apPat")+" "+datosGenerales.optString("apMat"));
				campos.add(element);
				element = new ContractItemDto();				
				element.setNombre("isCorreoElectronico");
				element.setValor(SELECT_MARCK);
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("fechaDia");
				element.setValor(day);
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("fechaMes");
				element.setValor(month);
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("fechaA");
				element.setValor(year);
				campos.add(element);
				log.info("> pag :"+ 2);//---------------------------------------->
				element = new ContractItemDto();
				element.setNombre("contratoNo");
				element.setValor("001");
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("nombres");
				element.setValor(datosGenerales.optString("nombres"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("aPaterno");
				element.setValor(datosGenerales.optString("apPat"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("aMaterno");
				element.setValor(datosGenerales.optString("apMat"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("profecionAct");
				element.setValor(datosGenerales.optString("ocupacion"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("fechaNacimiento");
				element.setValor(datosGenerales.optString("fechaNaci"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("nacionalidad");
				element.setValor(datosGenerales.optString("nacionalidad"));
				campos.add(element);
				element = new ContractItemDto();
				String residencia = datosGenerales.optBoolean("extranjera") ? "recidenciaExtranjero" : "recidenciaRepMex";
				element.setNombre(residencia);
				element.setValor(SELECT_MARCK);
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("rfcCurp");
				element.setValor(datosGenerales.optString("CURP"));
				campos.add(element);
				element = new ContractItemDto();
				String eCivil = datosGenerales.optBoolean("estadoCivil") ? "edoCivilCasado" : "edoCivilSoltero";// S;
				element.setNombre(eCivil);
				element.setValor(SELECT_MARCK);
				campos.add(element);
				element = new ContractItemDto();
				String matReg = datosGenerales.optBoolean("regimen") ? "casadoSepBienes" : "casadoSocConyugal";// "2";
				element.setNombre(matReg);
				element.setValor(SELECT_MARCK);
				campos.add(element);
				JSONObject datosDomicilio = (JSONObject) patrimonial.opt("DatosDomicilio");
				element.setNombre("calleNumero");
				element.setValor(datosDomicilio.optString("calle"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("colonia");
				element.setValor(datosDomicilio.optString("colonia"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("delMunicipio");
				element.setValor(datosDomicilio.optString("delegacion"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("estado");
				element.setValor(datosDomicilio.optString("estado"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("codigoPostal");
				element.setValor(datosDomicilio.optString("codigoP"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("pais");
				element.setValor(datosDomicilio.optString("pais"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("eMail");
				element.setValor(datosDomicilio.optString("email"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("telefonoCasa");
				element.setValor(datosGenerales.optString("numTel_Principal"));
				element.setNombre("telefonoOficina");
				element.setValor(datosGenerales.optString("segundoTel"));
				campos.add(element);
//				JSONObject intrdiccion = (JSONObject) patrimonial.opt("Intrdiccion");
//				org.json.JSONArray b4al = (org.json.JSONArray) intrdiccion.optJSONArray("Interdiccion_A");
//				org.json.JSONArray b4bl = (org.json.JSONArray) intrdiccion.optJSONArray("Interdicion_B");
//				org.json.JSONArray b4cl = (org.json.JSONArray) intrdiccion.optJSONArray("Interdicion_C");
//				Boolean b4a = (Boolean) b4al.opt(0);
//				Boolean b4b = (Boolean) b4bl.opt(0);
//				Boolean b4c = (Boolean) b4cl.opt(0);				
//				if (b4a)
//					element.setNombre("interA");
//					element.setValor(fideicomisoDTO.optString(SELECT_MARCK));
//					campos.add(element);
//				if (b4b)		
//					element.setNombre("interB");
//					element.setValor(fideicomisoDTO.optString(SELECT_MARCK));
//					campos.add(element);// B
//				if (b4c)
//					element.setNombre("interC");
//					element.setValor(fideicomisoDTO.optString(SELECT_MARCK));
//					campos.add(element);
				JSONObject casoFallesimiento = (JSONObject) patrimonial.opt("CasoFallesimiento");
				element = new ContractItemDto();
				element.setNombre("poderCobroA");
				element.setValor(casoFallesimiento.optString("Albacea"));
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("fechaCiudad");
				element.setValor("Ciudad de México");
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("fechaDia");
				element.setValor(day);
				campos.add(element);	
				element = new ContractItemDto();
				element.setNombre("fechaMes");
				element.setValor(month);
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("fechaA");
				element.setValor(year);
				campos.add(element);
				element = new ContractItemDto();
				element.setNombre("fideicomSr");
				element.setValor(datosGenerales.optString("nombres")+" "+datosGenerales.optString("apPat")+" "+datosGenerales.optString("apMat"));
				campos.add(element);
				element = new ContractItemDto();
				JSONObject datosGFamilia = (JSONObject) patrimonial.opt("DatosGFamilia");
				element.setNombre("conyuge");
				element.setValor(datosGFamilia.optString("nameConyuge"));
				campos.add(element);
				
				
				log.info("> pag :"+ 12);//---------------------------------------->
				JSONObject beneficiario = (JSONObject) fideicomisoDTO.opt("beneficiario");
				//TBL1
				JSONObject primerBenefi = (JSONObject) beneficiario.opt("PrimerBenefi");
				campos.addAll(llenatablaBenef(primerBenefi, 1));		
				//TBL2
				JSONObject segundoBenefi = (JSONObject) beneficiario.opt("SegundoBenefi");
				campos.addAll(llenatablaBenef(segundoBenefi, 2));		
				//TBL3
				JSONObject tercerBenefi = (JSONObject) beneficiario.opt("TercerBenefi");
				campos.addAll(llenatablaBenef(tercerBenefi, 3));
				//TBL4
				JSONObject cuartoBenefi = (JSONObject) beneficiario.opt("CuartoBenefi");
				campos.addAll(llenatablaBenef(cuartoBenefi, 4));
				//FIRMAS----------->>>
				element.setNombre("fideicomitente");
				element.setValor(datosGenerales.optString("nombres")+" "+datosGenerales.optString("apPat")+" "+datosGenerales.optString("apMat"));
				campos.add(element);
				element = new ContractItemDto();
				element.setPag(12);
				element.setNombre("conyuge");
				element.setValor(datosGFamilia.optString("nameConyuge"));
				campos.add(element);
				//HUELLAS----------------------------------->
//				String b64 = fideicomisoDTO.optString("huellaIndex");
//				String b64 = huellaIndexTemp;			
//				b64 = huellaIndexTemp.replace("\\\\", "\\");
				log.info("huellaIndexTemp>>>>>>>>>>>>>>>>>>>>>>>>>>"+huellaTemp);	
				if (huellaTemp != null && !huellaTemp.isEmpty()) {
					log.info("> pegandoHuella");
//					String huellaFinal = decrypt.decrypt(huellaTemp);
					element = new ContractItemDto();
					element.setNombre("huellaFidei1");
					element.setValor(huellaTemp);
					campos.add(element);
					element = new ContractItemDto();
					element.setNombre("huellaFidei2");
					element.setValor(huellaTemp);
					campos.add(element);
				}
				obj.setCodeContract("TMSFID");
				obj.setCampos(campos);						
				byte[] resByte = Base64.getDecoder().decode(contractService.getContract(obj));	
				return new ResponseEntity<>(resByte, HttpStatus.OK);
			} catch (Exception e) {
				log.error("Error"+e.getMessage());
				e.printStackTrace();
			}
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		private ArrayList<ContractItemDto> llenatablaBenef( JSONObject beneficiario, int tbl) {			
			ArrayList<ContractItemDto> list = new ArrayList<ContractItemDto>();
			ContractItemDto element = new ContractItemDto();
			element = new ContractItemDto();
			element.setNombre("FS"+tbl+"Nombre");
			element.setValor(beneficiario.optString("beneficiario"));
			list.add(element);
			element = new ContractItemDto();
			element.setNombre("FS"+tbl+"Parent");
			element.setValor(beneficiario.optString("parentesco"));
			list.add(element);
			element = new ContractItemDto();
			element.setNombre("FS"+tbl+"Porcent");
			element.setValor(beneficiario.optString("porcentaje"));
			list.add(element);	
			list.addAll(llenaTablaSubs(beneficiario.optJSONArray("HerederosSubs"), tbl));		
			return list;
		}
		
		private ArrayList<ContractItemDto> llenaTablaSubs(JSONArray sub,int tbl){
			ArrayList<ContractItemDto> list = new ArrayList<ContractItemDto>();
			if(sub.length()>0) {
				sub = new JSONArray(sub.toString().replaceAll("\\\\", ""));
			    log.info(sub.toString());
				JSONArray sub1 = (JSONArray) sub.get(0);
				for(int i = 0 ; i < sub1.length(); i ++ ) {
					ContractItemDto element = new ContractItemDto();
					JSONObject rowData = (JSONObject) sub1.get(i);
					element.setNombre("FS"+tbl+"Subs"+(i+1)+"Nombre");
					element.setValor(rowData.optString("nombre"));
					list.add(element);

					element = new ContractItemDto();
					element.setNombre("FS"+tbl+"Subs"+(i+1)+"Parent");
					element.setValor(rowData.optString("parentesco"));
					list.add(element);

					element = new ContractItemDto();
					element.setNombre("FS"+tbl+"Subs"+(i+1)+"Porcent");
					element.setValor(rowData.optString("porcentaje"));
					list.add(element);
				}
			}
			return list;
		}
		
		/**
		 * @return MMM_dd,_yyyy
		 */
		private String getDate() {
			SimpleDateFormat objSDF = new SimpleDateFormat("MMM_dd,_yyyy",  new Locale("es", "ES"));
			String date = objSDF.format(new Date());
			return date;
		}
		
		 private static byte[] loadFile(File file) throws IOException {
		        InputStream is = new FileInputStream(file);

		        long length = file.length();
		        if (length > Integer.MAX_VALUE) {
		            // File is too large
		        }
		        byte[] bytes = new byte[(int)length];

		        int offset = 0;
		        int numRead = 0;
		        while (offset < bytes.length
		               && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
		            offset += numRead;
		        }

		        if (offset < bytes.length) {
		            throw new IOException("Could not completely read file "+file.getName());
		        }

		        is.close();
		        return bytes;
		    }
	    
	  
	    
	    @ApiOperation(value = "Uploads contract signed and stores in the document manager")
	    @RequestMapping(value = "/contratoTms", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	    public ResponseEntity<String> getContratoTms(@RequestBody ContratoTmsDTO dto) 
	    {
	    	
	    	
	    	//log.info("lblancas: "+this.getClass().getName()+".{getContractDemo() }"+dto.toString());
	    	//log.info("Valores "+  dto.toString());
	    	byte[] responseEntity =null;
	    	try
	    	{
	    		responseEntity =getPdfContratoTms(dto);
	    	}catch(Exception e)
	    	{
	    		log.error(e.getMessage());
	    	}
	 
	    	return new ResponseEntity<>(Base64Utils.encodeToString( responseEntity) , HttpStatus.OK); 
	    }
	    
	  
	    
	    //---------creacion de pdf ----->
	    private byte[] getPdfContratoTms(ContratoTmsDTO dto)
	    {
	    	
	         ByteArrayOutputStream stream=null;
	         
	         try 
	         {
	        	 
	           Document document = new Document(PageSize.A4, 35, 30, 35, 50);
	                 
	           stream = new ByteArrayOutputStream();
	           PdfWriter.getInstance(document, stream);
	           
	           // Abrir el documento
	           document.open();
	           
	           Image image = null;
	         
	           // Crear las fuentes para el contenido y los titulos
	           
	           Font fontContenido = FontFactory.getFont(
	             FontFactory.TIMES_ROMAN.toString(), 11, Font.NORMAL,
	             BaseColor.DARK_GRAY);
	           
	           Font fontSubrayado = FontFactory.getFont(
	                     FontFactory.TIMES_ROMAN.toString(), 11, Font.UNDERLINE,
	                     BaseColor.DARK_GRAY);  
	           
	           Font fontTitulos = FontFactory.getFont(
	             FontFactory.HELVETICA, 9, Font.BOLD,
	             BaseColor.BLACK);
	           
	           Font fontContentDescrptivo = FontFactory.getFont(
	                   FontFactory.HELVETICA, 9,
	                     BaseColor.BLACK);
	           
	           Font tituloSubrayado = FontFactory.getFont(
	                     FontFactory.TIMES_BOLDITALIC, 9, Font.UNDERLINE,
	                     BaseColor.BLACK);
	           
	           
	           // Creacion del parrafo
	           Paragraph paragraph0 = new Paragraph();
	         
	           // Agregar un titulo con su respectiva fuente
	           paragraph0.add(new Phrase("Formato de autorización definido para las SOFOM E.N.R. (Entidades No Reguladas)", fontTitulos));
	           //log.info("Agregar saltos de linea");
	           // Agregar saltos de linea
	           paragraph0.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph0.add(new Phrase("Autorización para solicitar Reportes de Crédito", fontTitulos));
	           // Agregar saltos de linea
	           paragraph0.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph0.add(new Phrase("Personas Físicas / Personas Morales", fontTitulos));
	           paragraph0.add(new Phrase(Chunk.NEWLINE));
	           paragraph0.add(new Phrase(Chunk.NEWLINE));
	           paragraph0.add(new Phrase(Chunk.NEWLINE));
	           paragraph0.setAlignment(Element.ALIGN_CENTER);
	           paragraph0.setLeading(10);
	           document.add(paragraph0);
	           
	           
	           Paragraph paragraph1 = new Paragraph();
	           
	           paragraph1.add(new Phrase("Por este conducto autorizo expresamente a ", fontContentDescrptivo));
	           
	           paragraph1.add(new Phrase("CLICK SEGURIDAD JURIDICA SAPI DE CV SOFOM, E.N.R.", fontTitulos));
	           
	           paragraph1.add(new Phrase(" , para que por conducto de"
	           + " sus funcionarios facultados lleve a cabo Investigaciones, sobre mi comportamiento crediticio o el de la Empresa que represento en Trans"
	           + " Union de México, S. A. SIC y/o Dun & Bradstreet, S.A. SIC", fontContentDescrptivo));
	           
	           paragraph1.add(new Phrase(Chunk.NEWLINE));
	           paragraph1.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph1.add(new Phrase("Asimismo, declaro que conozco la naturaleza y alcance de las sociedades de información crediticia y de la información contenida en los"
	                   + " reportes de crédito y reporte de crédito especial, declaro que conozco la naturaleza y alcance de la información que se solicitará, del uso"
	                   + " que ", fontContentDescrptivo));
	           
	           paragraph1.add(new Phrase("CLICK SEGURIDAD JURIDICA SAPI DE CV SOFOM, E.N.R.", fontTitulos));
	                   
	           paragraph1.add(new Phrase(", hará de tal información y de que ésta podrá realizar consultas"
	           + " periódicas sobre mi historial o el de la empresa que represento, consintiendo que esta autorización se encuentre vigente por un período"
	           + " de 3 años contados a partir de su expedición y en todo caso durante el tiempo que se mantenga la relación jurídica.", fontContentDescrptivo));
	           paragraph1.add(new Phrase(Chunk.NEWLINE));
	           paragraph1.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph1.add(new Phrase("En caso de que la solicitante sea una Persona Moral, declaro bajo protesta de decir verdad Ser Representante Legal de la empresa"
	                   + " mencionada en esta autorización; manifestando que a la fecha de firma de la presente autorización los poderes no me han sido"
	                   + " revocados, limitados, ni modificados en forma alguna.", fontContentDescrptivo));
	           paragraph1.add(new Phrase(Chunk.NEWLINE));
	           paragraph1.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph1.setLeading(10);
	           document.add(paragraph1);
	           
	           Paragraph paragraph2 = new Paragraph();
	           paragraph2.add(new Phrase("Autorización para:", fontContentDescrptivo));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase("Persona Física (PF)  ___" + " Persona Física con Actividad Empresarial (PFAE)  ___" + " Persona Moral  (PM)  ___", fontTitulos));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph2.add(new Phrase("Nombre del solicitante (Persona Física o Razón Social de la Persona Moral):", fontTitulos));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase(dto.getNombre() +" "+ dto.getApPat() +" "+ dto.getApMat(), fontSubrayado));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph2.add(new Phrase("Para el caso de Persona Moral, nombre del Representante Legal:", fontTitulos));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph2.add(new Phrase("RFC o CURP: ", fontTitulos));
	           paragraph2.add(new Phrase(dto.getCurp() , fontSubrayado));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           
	           String domicilio = dto.getCalle().concat(" "+dto.getNumExt()+" "+dto.getNumInt());
	           paragraph2.add(new Phrase("Domicilo: ", fontContentDescrptivo));
	           paragraph2.add(new Phrase(domicilio , fontSubrayado));
	           paragraph2.add(new Phrase("    Colonia: ", fontContentDescrptivo));
	           paragraph2.add(new Phrase(dto.getColonia() , fontSubrayado));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph2.add(new Phrase("Municipio: ", fontContentDescrptivo));
	           paragraph2.add(new Phrase(dto.getMunicipio() , fontSubrayado));
	           paragraph2.add(new Phrase("    Estado: ", fontContentDescrptivo));
	           paragraph2.add(new Phrase(dto.getEstado() , fontSubrayado));
	           paragraph2.add(new Phrase("    Código postal: ", fontContentDescrptivo));
	           paragraph2.add(new Phrase(dto.getCodigoP(), fontSubrayado));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph2.add(new Phrase("Teléfono:  ", fontContentDescrptivo));
	           paragraph2.add(new Phrase(dto.getNumTel_Principal(), fontSubrayado));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           paragraph2.add(new Phrase(Chunk.NEWLINE));
	           document.add(paragraph2);
	           
	           Date d = new Date();
	           Calendar c = new GregorianCalendar(); 
	           c.setTime(d);

	           String dia, mes, anio;

	           dia = Integer.toString(c.get(Calendar.DATE));
	           mes = Integer.toString(c.get(Calendar.MONTH)+1);
	           anio = Integer.toString(c.get(Calendar.YEAR));

	           Paragraph paragraph = new Paragraph();
	           String[] date = getDate().split("_");
	           mes = date[0].substring(0, 1).toUpperCase()+""+date[0].substring(1,date[0].length());
	           paragraph.add(new Phrase("Lugar y Fecha en que se firma la autorización:  " + "Ciudad de México de "+ date[1] +" de "+ mes + " de " + date[2] , fontTitulos));
	           paragraph.add(new Phrase(Chunk.NEWLINE));
	           paragraph.add(new Phrase(Chunk.NEWLINE));
	           
	           paragraph.add(new Phrase("Nombre del funcionario que recaba la autorización:  _______________", fontTitulos));
	           paragraph.add(new Phrase(Chunk.NEWLINE));
	           paragraph.add(new Phrase(Chunk.NEWLINE));
	           document.add(paragraph);
	           
	           Paragraph paragraph4 = new Paragraph();
	           paragraph4.add(new Phrase("Estoy consciente y acepto que este documento quede bajo custodia de CLICK SEGURIDAD JURIDICA SAPI DE CV SOFOM, "
	                   + "E.N.R  y/o Sociedad de Información Crediticia consultada para efectos de control y cumplimiento del artículo 28 de la Ley para "
	                   + "Regular las Sociedades de Información Crediticia; mismo que señala que las Sociedades sólo podrán proporcionar "
	                   + "información a un Usuario, cuando éste cuente con la autorización expresa del Cliente mediante su firma autógrafa.", fontTitulos));
	           paragraph4.setLeading(10);
	           paragraph4.add(new Phrase(Chunk.NEWLINE));
	           paragraph4.add(new Phrase(Chunk.NEWLINE));
	           paragraph4.add(new Phrase(Chunk.NEWLINE));
	           document.add(paragraph4);
	            
	           //log.info(" Tabla PDF....");  
	           PdfPTable table = new PdfPTable(5);
	           float dimension[]={80,80,40,30,80};
	           table.setTotalWidth(dimension);
	           // Agregar la imagen anterior a una celda de la tabla
	           PdfPCell cel10 = new PdfPCell(new Phrase(" "));
	           cel10.setFixedHeight(50);
	           cel10.setBorder(Rectangle.NO_BORDER);
	           cel10.setColspan(1);
	           table.addCell(cel10);
	           
	           Paragraph FIRMA = new Paragraph();
	           FIRMA.add(new Phrase(dto.getNombre() +" "+ dto.getApPat() +" "+ dto.getApMat(), tituloSubrayado));
	           FIRMA.add(new Phrase(Chunk.NEWLINE));    
	           FIRMA.add(new Phrase(Chunk.NEWLINE));
	           FIRMA.add(new Phrase("Firma de PF, PFAE o Representante Legal de la empresa", fontTitulos));
	           FIRMA.setAlignment(Element.ALIGN_CENTER);
	           PdfPCell cel20 = new PdfPCell(FIRMA);
	           cel20.setColspan(2);
	           cel20.setBorder(Rectangle.NO_BORDER);
	           cel20.setFixedHeight(50);
	           table.addCell(cel20);
	           table.setWidthPercentage(120f);
	           
	           if(dto.isConhuella())
	           {
	        	   
	               try
	               {
	            	   
	            	String huellaFinal = decrypt.decrypt(dto.getIndexLeft());
	   	        	byte[] imageByte;
	   	        	
	   	    		imageByte = Base64Utils.decodeFromString(huellaFinal);
	   	    		File rutaHuella = new File("/home/fingerToFirma.png");
	   	    		
	   	    		if(rutaHuella.exists()) {
	   	    			
	   	    			rutaHuella.delete();
	   	    			rutaHuella = new File("/home/fingerToFirma.png");
	   	    		}
	   	    		
	   	    		InputStream in = new ByteArrayInputStream(imageByte);
	   				BufferedImage bImageFromConvert = ImageIO.read(in);
	   	    		
	   	    		ImageIO.write(bImageFromConvert, "png", rutaHuella );

	            	   
	                   PdfPCell cel30 =  null; 
	                 image = Image.getInstance("/home/fingerToFirma.png");
	                   image.scaleAbsolute(50f, 70f);
	                   cel30= new PdfPCell(image); 
	                   cel30.setBorder(Rectangle.NO_BORDER);
	                   cel30.setColspan(1);
	                   cel30.setFixedHeight(70);
	                   table.addCell(cel30);
	                   
	               }catch(Exception e){
	            	   
	                   PdfPCell cel30 =  null; 
	                   
	                   image = Image.getInstance("\\home\\fingerToFirma.png");
	                   
	                   image.scaleAbsolute(50f, 50f);
	                   cel30= new PdfPCell(image); 
	                   cel30.setBorder(Rectangle.NO_BORDER);
	                   cel30.setColspan(1);
	                   cel30.setFixedHeight(50);
	                   table.addCell(cel30);
	               }
	           }
	           else
	           {
	               PdfPCell cel30 = new PdfPCell(new Phrase(" "));
	               cel30.setBorder(Rectangle.NO_BORDER);
	               cel30.setColspan(1);
	               cel30.setFixedHeight(50);
	               table.addCell(cel30); 
	           }
	           
	           PdfPCell cel40 = new PdfPCell(new Phrase(" "));
	           cel40.setFixedHeight(50);
	           cel40.setBorder(Rectangle.NO_BORDER);
	           cel40.setColspan(1);
	           table.addCell(cel40);  
	           //log.info(" Crea tabla "); 
	           
	           document.add(table);
	           
	           PdfPTable tabla = new PdfPTable(1);
	           PdfPCell celda1 = new PdfPCell();
	           celda1.addElement(new Phrase("Para uso exclusivo de la Empresa que efectúa la consulta CLICK SEGURIDAD JURIDICA SAPI DE CV SOFOM, E.N.R ", fontTitulos));
	           celda1.addElement(new Phrase("Fechas de Consulta BC:  _____", fontContentDescrptivo));
	           celda1.addElement(new Phrase("Folio de Consulta BC: _____", fontContentDescrptivo));
	           celda1.addElement(new Phrase(Chunk.NEWLINE));
	           celda1.setBackgroundColor(BaseColor.LIGHT_GRAY);
	           celda1.setFixedHeight(50f);
	           tabla.setWidthPercentage(99f);
	           tabla.addCell(celda1);
	           document.add(tabla);
	           
	           Paragraph paragraph3 = new Paragraph();
	           paragraph3.add(new Phrase(Chunk.NEWLINE));
	           paragraph3.add(new Phrase("IMPORTANTE: ", tituloSubrayado));
	           paragraph3.add(new Phrase("Este formato debe ser llenado individualmente, para una sola persona física ó para una sola empresa. En caso de requerir el Historial crediticio del representante legal, favor de llenar un formato adicional.", fontContentDescrptivo));
	           paragraph3.setLeading(10);
	           document.add(paragraph3);
	           
	           // Cerrar el documento
	           document.close();
	           //Desktop.getDesktop().open();
	          } catch (Exception ex) {
	        	  
	           ex.printStackTrace();
	          }
//	       log.info(" fin "); 
	         return stream.toByteArray();
	    }
	    
	    
		@ApiOperation(value = "Finds the values that must be accepted by the customer", response = AcceptancePerCustomerDTO.class)
		@RequestMapping(value = "/manualSiginService", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<String> manualSiginService(@RequestPart(value = "doc") MultipartFile doc,	@RequestPart(value = "img") MultipartFile img, @RequestBody String request) {	
			log.info("INFO : " + this.getClass().getName() + "manualSiginService ");
			if(doc.getName()!=null)
			log.info("RECIVED FILE > " + doc.getName());
			else return new ResponseEntity<>("Error: empty doc", HttpStatus.BAD_REQUEST);
			if(img.getName()!=null)
			log.info("RECIVED FILE > " + img.getName());
			else return new ResponseEntity<>("Error: empty img", HttpStatus.BAD_REQUEST);
			if(request!=null&&!request.isEmpty())
			log.info("RECIVED DATA > " + request);
			else return new ResponseEntity<>("Error: empty data", HttpStatus.BAD_REQUEST);	
	    	//TODO obtener contrato
	    	
	    	//pegar huellas
	    	
	    	//firmar Karalundi
	    	
	    	// regresar b64
	    	
	    	return new ResponseEntity<>("" , HttpStatus.OK); 
		}

}
