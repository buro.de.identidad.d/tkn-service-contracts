package com.teknei.bid.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.tempuri.Doc2SignLiteLocator;
import org.tempuri.IDoc2SignLite;

import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClie;


@Component
public class KaralundiSignService {

    private static final Logger log = LoggerFactory.getLogger(KaralundiSignService.class);
  
    @Autowired
    private ContractTasService contractTasService;
    
    private IDoc2SignLite service;
    
    private String user;
    private String pass;   
    private String endpoint;    
    		
	@PostConstruct
	private void postConstruct() {
		try {
			user = "d89fa5e7-8df3-4a89-a5be-368fed9e2a8b";
			pass = "7f53e04c-37b5-4965-9ec4-a33d6f07ed07";
			endpoint = "https://wstestautosign.doc2sign.com/Doc2SignLite.svc";
			Doc2SignLiteLocator ser = new Doc2SignLiteLocator();
			ser.setBasicHttpBinding_IDoc2SignLiteEndpointAddress(endpoint);
			service = ser.getBasicHttpBinding_IDoc2SignLite();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
    
	
    public CommandResponse firmadoKaralundi(Long idTas, String contractBase64, BidClie bidClie, String email) {
    	CommandResponse response = new CommandResponse(); 
        String identificador = UUID.randomUUID().toString();     
        try 
        {
            //Informacion para firmado.
            log.info("INFO: Firmando contrato.. ");                       
            log.info("Nombres>"+bidClie.getNomClie());
            log.info("ApPaterno>"+bidClie.getApePate());
            log.info("ApMaterno>"+bidClie.getApeMate());
            log.info("RFC>"+bidClie.getRfc());
            log.info("mail>"+email);
            log.info("Identificador>"+identificador);
            log.info("length : "+(contractBase64.length()));
            
            //Firmando documento ..
			String resultado = service.load(user, pass, identificador, bidClie.getNomClie(), bidClie.getApePate(),
					bidClie.getApeMate(), bidClie.getRfc(), email, contractBase64, false, "");
			log.info("INFO: Respuesta Load: " + resultado);

            //Obteniendo Documento Firmando ..
			String docFirmado = service.GETDocFirmado(user, pass, identificador);
			log.info("INFO: Resultado  GETDocFirmado : "+docFirmado.length());	            
            writeInFile("pdf.encode",docFirmado); 
            byte[] contractSigned = (byte[] )Base64Utils.decode(docFirmado.getBytes());            
            writeInFile("pdf",contractSigned);
            
            //Firmando en NOM151 ..
            String docNom151 = service.GETNOM(user, pass, identificador);                        
            log.info("Resultado GETNOM : "+docNom151.length());
            writeInFile("data.encode",docNom151);
            byte[] nom151= (byte[] )Base64Utils.decode(docNom151.getBytes());
            writeInFile("data",contractSigned);
            
            //Agregando a TAS ..
            contractTasService.addContactSignedToTas(contractSigned, idTas);
			if (nom151 != null) {
				try {
					contractTasService.addContactNom151EvidencetToTas(nom151, idTas);
					log.info("Added NOM151 to TAS services");
				} catch (Exception e) {
					log.error("Error adding NOM151 TAS service");
				}
			} else {
				log.info("NOM151 null");
			}			
            
            response.setSignedContract(contractSigned);
            contractBase64 = "";
            
        } catch (Exception e) {
            log.error("Error signing contract for request: {} with message: {}", identificador, e.getMessage());
            response.setStatus(Status.CONTRACT_OK);
            saveFileIfFail(contractBase64);
        }
        return response;
    }
    
    
    private void saveFileIfFail(String contractoNoFirmado) {
    	if(!contractoNoFirmado.isEmpty()) {
    		log.info(">Guardando pdf..");
    		writeInFile("pdf",contractoNoFirmado);
    	} 	
    }
    
    
    private void writeInFile(String ext, String body)
    {
    	String fileName=""+(new Date()).getTime()+"."+ext;
    	String ruta="/home/";
    	{	
    		try {
    			File file = new File(ruta+fileName);
    			log.info(file.getAbsolutePath());
    			FileWriter fileWriter = new FileWriter(file);
    			fileWriter.write(body);
    			fileWriter.flush();
    			fileWriter.close();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    }
    public void writeInFile(String  ext,byte[] strToBytes)
    {
    	String fileName=""+(new Date()).getTime()+"."+ext;
    	String ruta="/home/";
        FileOutputStream outputStream;
		try {
			outputStream = new FileOutputStream(ruta+fileName);
			outputStream.write(strToBytes);
	        outputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }

}
