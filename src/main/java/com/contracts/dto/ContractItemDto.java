package com.contracts.dto;


public class ContractItemDto {

	private int pag;
	private String nombre;
	private String valor;
	private boolean isImg;
	private int x;
	private int y;
	private String fontStyle;
	private int fontSize;
	private int alignment;//LEFT = 0, CENTER = 1,RIGHT = 2
	private float imageWidth;
	private float imageHeight;
	
	public int getPag() {
		return pag;
	}
	public void setPag(int pag) {
		this.pag = pag;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public boolean isImg() {
		return isImg;
	}
	public void setImg(boolean isImg) {
		this.isImg = isImg;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getFontStyle() {
		return fontStyle;
	}
	public void setFontStyle(String fontStyle) {
		this.fontStyle = fontStyle;
	}
	public int getFontSize() {
		return fontSize;
	}
	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
	}
	public int getAlignment() {
		return alignment;
	}
	public void setAlignment(int alignment) {
		this.alignment = alignment;
	}
	public float getImageWidth() {
		return imageWidth;
	}
	public void setImageWidth(float imageWidth) {
		this.imageWidth = imageWidth;
	}
	public float getImageHeight() {
		return imageHeight;
	}
	public void setImageHeight(float imageHeight) {
		this.imageHeight = imageHeight;
	}
	
	@Override
	public String toString() {
		return "ContractItemDto [pag=" + pag + ", nombre=" + nombre + ", valor=" + valor + ", isImg=" + isImg + ", x="
				+ x + ", y=" + y + ", fontStyle=" + fontStyle + ", fontSize=" + fontSize + ", alignment=" + alignment
				+ ", imageWidth=" + imageWidth + ", imageHeight=" + imageHeight + "]";
	}

}
