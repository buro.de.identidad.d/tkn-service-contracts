package com.contracts.dto;


import java.util.List;

public class RequestContractDto {

	private String codeContract;
	
	private String idTas;

	private List<ContractItemDto> campos;
	
	public String getIdTas() {
		return idTas;
	}

	public void setIdTas(String idTas) {
		this.idTas = idTas;
	}

	public String getCodeContract() {
		return codeContract;
	}

	public void setCodeContract(String codeContract) {
		this.codeContract = codeContract;
	}

	public List<ContractItemDto> getCampos() {
		return campos;
	}

	public void setCampos(List<ContractItemDto> campos) {
		this.campos = campos;
	}
	
}