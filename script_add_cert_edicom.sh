#!/bin/sh
echo -n | openssl s_client -connect ecsweb.sedeb2b.com:9125 | \
  sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /tmp/abc_1.crt
openssl x509 -in /tmp/abc_1.crt -text
keytool -import -trustcacerts -keystore /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/security/cacerts \
 -storepass changeit -noprompt -alias edicom -file /tmp/abc_1.crt
