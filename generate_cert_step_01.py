#!/usr/bin/python
import os
import sys
import tarfile
from subprocess import call
import shutil


def generateCertificate(absPath, customerName, customerId, sslTarfile, wsdl, mail, username, password, certPass, isTest):
  print "Begin generate certificate"
  folderCustomerUri = absPath+"/"+customerName+"-"+customerId
  print "Making folder: "+folderCustomerUri
  if os.path.exists(folderCustomerUri):
    print "Folder already exists, deleting..."
    shutil.rmtree(folderCustomerUri)
  os.makedirs(folderCustomerUri)
  print "Folders made"
  print "Extracting certificate"
  with tarfile.open(sslTarfile) as tar:
    tar.extractall(path=folderCustomerUri)
  print "Extraction made"
  certFile = folderCustomerUri + "/BURO_SSL.P12"
  jarFile = absPath + "/tkn-gen-cert.jar"
  customerOutputDir = folderCustomerUri
  print "All ready, calling routine for cert generation"
  print "About to call: java -jar "+wsdl+" "+certFile+" "+certPass+" "+customerName+" "+customerId+" "+mail+" "+username+" "+password+" "+customerOutputDir+" "+isTest
  return_code = call(["java", "-jar", jarFile, wsdl, certFile, certPass, customerName, customerId, mail, username, password, customerOutputDir, isTest])
  print "End generate certificate with status code :"+str(return_code)
  error_readed_uri = customerOutputDir + "/"+str(customerId) + username + "_error.code"
  error_code_written = ''
  try:
    with open(error_readed_uri, 'r') as myfile:
      error_code_written=myfile.read().replace('\n', '')
      return error_code_written
  except:
      print "No error readed in file, returning error code from system"
      return return_code

########################################################################################################

print "##########PYTHON_PROCESS##############"
print "Starting selfsigned certificate"

#Defining args
absPath = sys.argv[1] #/home/amaro/Documentos/TKN/user_cert - Must have here SSL.tar.gz and JAR files
customerName = sys.argv[2] #JORGE_AMARO_CORIA
customerId = sys.argv[3] #1
sslTarfile = sys.argv[4] #/home/amaro/Documentos...../SSL.tar.gz
wsdlFile01 = sys.argv[5] #http.....
mail = sys.argv[6] #jamaro@teknei.com
username = sys.argv[7] #jamaro
password = sys.argv[8] #amaro123
certPass = sys.argv[9]
isTestArgv = sys.argv[10]

ret_code = generateCertificate(absPath, customerName, customerId, sslTarfile, wsdlFile01, mail, username, password, certPass, isTestArgv)
print "Exit with code: " + str(ret_code)
print "###########PYTHON_PROCESS_END############"
raise SystemExit(ret_code)